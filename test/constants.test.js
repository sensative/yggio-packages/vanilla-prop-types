'use strict';

// this is included mainly as a secondary manifest

const _ = require('lodash');
const assert = require('assert');

const allConstants = require('../src/constants');
const {
  MESSAGE_ITEMS_CROP,
  PROP_TYPES_IDENTIFIER_KEYS,
  MAIN_OPTIONS_KEYS,
  SHAPE_OPTIONS_KEYS,
  CHAINED_ERROR_OPTIONS,
} = allConstants;

describe('constants', () => {

  it('size of allConstants', () => {
    assert.strictEqual(_.size(allConstants), 5);
  });

  it('MESSAGE_ITEMS_CROP', () => {
    assert.strictEqual(MESSAGE_ITEMS_CROP, 3);
  });

  it('PROP_TYPES_IDENTIFIER_KEYS', () => {
    assert.strictEqual(_.size(PROP_TYPES_IDENTIFIER_KEYS), 5);
  });

  it('SHAPE_OPTIONS_KEYS', () => {
    assert.strictEqual(_.size(SHAPE_OPTIONS_KEYS), 3);
  });

  it('CHAINED_ERROR_OPTIONS', () => {
    assert.strictEqual(_.size(CHAINED_ERROR_OPTIONS), 1);
  });

  it('MAIN_OPTIONS_KEYS', () => {
    assert.strictEqual(_.size(MAIN_OPTIONS_KEYS), 10);
  });

});
