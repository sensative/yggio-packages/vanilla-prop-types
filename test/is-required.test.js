'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
} = require('../src');

describe('isRequired, isRequiredOrNull, isForbidden', () => {

  describe('isRequired', () => {

    it('non-functional & nilly generator args work (the same)', () => {
      const nillyValidators = [
        PropTypes.string.isRequired,
        PropTypes.string.isRequired(),
        PropTypes.string.isRequired(null),
        PropTypes.string.isRequired(undefined),
      ];
      _.each(nillyValidators, NillyValidator => {
        const validate = validateProps(NillyValidator);
        assert.doesNotThrow(() => validate('asdf'));
        const mess = /PropTypes validation error: Prop isRequired \(cannot be null or undefined\)/;
        assert.throws(() => validate(null), mess);
        assert.throws(() => validate(undefined), mess);
      });
    })

    it('functional with INVALID arg FAILS', () => {
      const mess = /VanillaPropTypesInitializationError: Initialization Error: "isRequired" checker generator must include an argument \(if not nil\) that is either a string or error or be a function that generates an error/;
      assert.throws(() => validateProps(PropTypes.string.isRequired(23)), mess);
    });

    it('functional with string arg works', () => {
      const customMess = 'custom message';
      const validate = validateProps(PropTypes.string.isRequired(customMess));
      assert.doesNotThrow(() => validate('asdf'));
      const mess = /PropTypes validation error: custom message/;
      assert.throws(() => validate(null), mess);
      assert.throws(() => validate(undefined), mess);
    });

    it('functional with error arg works', () => {
      const customErr = new Error('custom error');
      const validate = validateProps(PropTypes.string.isRequired(customErr));
      assert.doesNotThrow(() => validate('asdf'));
      const mess = /custom error/;
      assert.throws(() => validate(null), mess);
      assert.throws(() => validate(undefined), mess);
      // ensure strict error equality
      try {
        validate()
      } catch (err) {
        assert.strictEqual(err, customErr);
        assert.strictEqual(err.message, 'custom error');
      }
    });

    it('functional with string-generator arg works', () => {
      const customMess = 'custom message';
      const validate = validateProps(PropTypes.string.isRequired(() => customMess));
      assert.doesNotThrow(() => validate('asdf'));
      const mess = /PropTypes validation error: custom message/;
      assert.throws(() => validate(null), mess);
      assert.throws(() => validate(undefined), mess);
    });

    it('functional with error-generator arg works', () => {
      const customErr = new Error('custom error');
      const validate = validateProps(PropTypes.string.isRequired(() => customErr));
      assert.doesNotThrow(() => validate('asdf'));
      const mess = /custom error/;
      assert.throws(() => validate(null), mess);
      assert.throws(() => validate(undefined), mess);
      // ensure strict error equality
      try {
        validate()
      } catch (err) {
        assert.strictEqual(err, customErr);
        assert.strictEqual(err.message, 'custom error');
      }
    });

  });

  describe('isRequiredOrNull', () => {

    it('non-functional & nilly generator args work (the same)', () => {
      const nillyValidators = [
        PropTypes.string.isRequiredOrNull,
        PropTypes.string.isRequiredOrNull(),
        PropTypes.string.isRequiredOrNull(null),
        PropTypes.string.isRequiredOrNull(undefined),
      ];
      _.each(nillyValidators, NillyValidator => {
        const validate = validateProps(NillyValidator);
        assert.doesNotThrow(() => validate('asdf'));
        assert.doesNotThrow(() => validate(null));
        const mess = /PropTypes validation error: Prop isRequiredOrNull \(cannot be undefined\)/;
        assert.throws(() => validate(undefined), mess);
      });
    })

    it('functional with INVALID arg FAILS', () => {
      const mess = /VanillaPropTypesInitializationError: Initialization Error: "isRequiredOrNull" checker generator must include an argument \(if not nil\) that is either a string or error or be a function that generates an error/;
      assert.throws(() => validateProps(PropTypes.string.isRequiredOrNull(23)), mess);
    });

    it('functional with string arg works', () => {
      const customMess = 'custom message';
      const validate = validateProps(PropTypes.string.isRequiredOrNull(customMess));
      assert.doesNotThrow(() => validate('asdf'));
      assert.doesNotThrow(() => validate(null));
      const mess = /PropTypes validation error: custom message/;
      assert.throws(() => validate(undefined), mess);
    });

    it('functional with error arg works', () => {
      const customErr = new Error('custom error');
      const validate = validateProps(PropTypes.string.isRequiredOrNull(customErr));
      assert.doesNotThrow(() => validate('asdf'));
      assert.doesNotThrow(() => validate(null));
      const mess = /custom error/;
      assert.throws(() => validate(undefined), mess);
      // ensure strict error equality
      try {
        validate()
      } catch (err) {
        assert.strictEqual(err, customErr);
        assert.strictEqual(err.message, 'custom error');
      }
    });

    it('functional with string-generator arg works', () => {
      const customMess = 'custom message';
      const validate = validateProps(PropTypes.string.isRequiredOrNull(() => customMess));
      assert.doesNotThrow(() => validate('asdf'));
      assert.doesNotThrow(() => validate(null));
      const mess = /PropTypes validation error: custom message/;
      assert.throws(() => validate(undefined), mess);
    });

    it('functional with error-generator arg works', () => {
      const customErr = new Error('custom error');
      const validate = validateProps(PropTypes.string.isRequiredOrNull(() => customErr));
      assert.doesNotThrow(() => validate('asdf'));
      assert.doesNotThrow(() => validate(null));
      const mess = /custom error/;
      assert.throws(() => validate(undefined), mess);
      // ensure strict error equality
      try {
        validate()
      } catch (err) {
        assert.strictEqual(err, customErr);
        assert.strictEqual(err.message, 'custom error');
      }
    });

  });


  describe('isForbidden', () => {


    it('non-functional & nilly generator args work (the same)', () => {
      const forbiddenValidators = [
        PropTypes.isForbidden,
        PropTypes.isForbidden(),
        PropTypes.isForbidden(null),
        PropTypes.isForbidden(undefined),
      ];
      _.each(forbiddenValidators, ForbiddenValidator => {
        const validate = validateProps({asdf: ForbiddenValidator});
        assert.doesNotThrow(() => validate({asdf: null}));
        assert.doesNotThrow(() => validate({asdf: undefined}));
        const mess = /PropTypes validation error at \[asdf]: Prop isForbidden \(must be either undefined or null\)/;
        assert.throws(() => validate({asdf: 23}), mess);
        assert.throws(() => validate({asdf: 'anything not undefined or null'}), mess);
      });
    });

    it('non-functional & nilly - can even be used directly (trivial case)', () => {
      const forbiddenValidators = [
        PropTypes.isForbidden,
        PropTypes.isForbidden(),
        PropTypes.isForbidden(null),
        PropTypes.isForbidden(undefined),
      ];
      _.each(forbiddenValidators, ForbiddenValidator => {
        const validate = validateProps(ForbiddenValidator);
        assert.doesNotThrow(() => validate(null));
        assert.doesNotThrow(() => validate(undefined));
        const mess = /PropTypes validation error: Prop isForbidden \(must be either undefined or null\)/;
        assert.throws(() => validate(23), mess);
        assert.throws(() => validate('anything not undefined or null'), mess);
      });
    });

    it('functional with INVALID arg FAILS', () => {
      const mess = /Initialization Error: "isForbidden" checker generator must include an argument \(if not nil\) that is either a string or error or be a function that generates an error/;
      assert.throws(() => validateProps(PropTypes.isForbidden(23)), mess);
    });

    it('functional with string arg works', () => {
      const customMess = 'custom message';
      const validate = validateProps(PropTypes.isForbidden(customMess));
      assert.doesNotThrow(() => validate(null));
      assert.doesNotThrow(() => validate(undefined));
      const mess = /PropTypes validation error: custom message/;
      assert.throws(() => validate(23), mess);
      assert.throws(() => validate('anything not undefined or null'), mess);
    });

    it('functional with error arg works', () => {
      const customErr = new Error('custom error');
      const validate = validateProps(PropTypes.isForbidden(customErr));
      assert.doesNotThrow(() => validate(null));
      assert.doesNotThrow(() => validate(undefined));
      const mess = /custom error/;
      assert.throws(() => validate(23), mess);
      assert.throws(() => validate('anything not undefined or null'), mess);
      // ensure strict error equality
      try {
        validate(23)
      } catch (err) {
        assert.strictEqual(err, customErr);
        assert.strictEqual(err.message, 'custom error');
      }
    });

    it('functional with string-generator arg works', () => {
      const customMess = 'custom message';
      const validate = validateProps(PropTypes.isForbidden(() => customMess));
      assert.doesNotThrow(() => validate(null));
      assert.doesNotThrow(() => validate(undefined));
      const mess = /PropTypes validation error: custom message/;
      assert.throws(() => validate(23), mess);
      assert.throws(() => validate('anything not undefined or null'), mess);
    });

    it('functional with error-generator arg works', () => {
      const customErr = new Error('custom error');
      const validate = validateProps(PropTypes.isForbidden(() => customErr));
      assert.doesNotThrow(() => validate(null));
      assert.doesNotThrow(() => validate(undefined));
      const mess = /custom error/;
      assert.throws(() => validate(23), mess);
      assert.throws(() => validate('anything not undefined or null'), mess);
      // ensure strict error equality
      try {
        validate(23)
      } catch (err) {
        assert.strictEqual(err, customErr);
        assert.strictEqual(err.message, 'custom error');
      }
    });

  });

});
