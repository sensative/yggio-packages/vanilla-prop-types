'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../src');

describe('simple single props', () => {

  describe('any', () => {

    it('any', () => {
      const check = checkProps({anyProp: PropTypes.any});
      const mess0 = check({anyProp: 'a string or anything should work'});
      assert.strictEqual(mess0, null);
      const mess2 = check({});
      assert.strictEqual(mess2, null);
      const mess3 = check({anyProp: undefined});
      assert.strictEqual(mess3, null);
      const mess4 = check({anyProp: null});
      assert.strictEqual(mess4, null);
    });

    it('any.isRequired', () => {
      const check = checkProps({anyProp: PropTypes.any.isRequired});
      const mess1 = check({anyProp: 'a string or anything should work'});
      assert.strictEqual(mess1, null);
      const mess2 = check({});
      assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({anyProp: undefined});
      assert.strictEqual(mess3, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({anyProp: null});
      assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
    });

    it('any.isRequiredOrNull', () => {
      const check = checkProps({anyProp: PropTypes.any.isRequiredOrNull});
      const mess1 = check({anyProp: 'a string or anything should work'});
      assert.strictEqual(mess1, null);
      const mess2 = check({});
      assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({anyProp: undefined});
      assert.strictEqual(mess3, 'PropTypes validation error at [anyProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess4 = check({anyProp: null});
      assert.strictEqual(mess4, null);
    });

    it('any.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({anyProp: PropTypes.any.custom(customFn)});
      const mess1 = check({anyProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({anyProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({anyProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({anyProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
    });

    it('any.isRequired.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({anyProp: PropTypes.any.isRequired.custom(customFn)});
      const mess1 = check({anyProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({anyProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({anyProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({anyProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
    });

    it('any.isRequiredOrNull.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({anyProp: PropTypes.any.isRequiredOrNull.custom(customFn)});
      const mess1 = check({anyProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({anyProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({anyProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({anyProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
    });

    it('any.custom.isRequired', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({anyProp: PropTypes.any.custom(customFn).isRequired});
      const mess1 = check({anyProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({anyProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({anyProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [anyProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({anyProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
    });

    it('any.custom.isRequiredOrNull', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({anyProp: PropTypes.any.custom(customFn).isRequiredOrNull});
      const mess1 = check({anyProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({anyProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [anyProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({anyProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({anyProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [anyProp]: custom err mess');
    });

  });

  describe('string', () => {

    it('string', () => {
      const check = checkProps({stringProp: PropTypes.string});
      const mess0 = check({stringProp: 'a string should work'});
      assert.strictEqual(mess0, null);
      const mess1 = check({stringProp: new String('a String should also work')});
      assert.strictEqual(mess1, null);
      const mess2 = check({});
      assert.strictEqual(mess2, null);
      const mess3 = check({stringProp: undefined});
      assert.strictEqual(mess3, null);
      const mess4 = check({stringProp: null});
      assert.strictEqual(mess4, null);
      const mess5 = check({stringProp: 23});
      assert.strictEqual(mess5, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
      const mess6 = check({stringProp: {a: 2}});
      assert.strictEqual(mess6, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
    });

    it('string.isRequired', () => {
      const check = checkProps({stringProp: PropTypes.string.isRequired});
      const mess1 = check({stringProp: 'a string should work'});
      assert.strictEqual(mess1, null);
      const mess2 = check({});
      assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({stringProp: undefined});
      assert.strictEqual(mess3, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({stringProp: null});
      assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
      const mess5 = check({stringProp: 23});
      assert.strictEqual(mess5, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
      const mess6 = check({stringProp: {a: 2}});
      assert.strictEqual(mess6, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
    });

    it('string.isRequiredOrNull', () => {
      const check = checkProps({stringProp: PropTypes.string.isRequiredOrNull});
      const mess1 = check({stringProp: 'a string should work'});
      assert.strictEqual(mess1, null);
      const mess2 = check({});
      assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({stringProp: undefined});
      assert.strictEqual(mess3, 'PropTypes validation error at [stringProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess4 = check({stringProp: null});
      assert.strictEqual(mess4, null);
      const mess5 = check({stringProp: 23});
      assert.strictEqual(mess5, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
      const mess6 = check({stringProp: {a: 2}});
      assert.strictEqual(mess6, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
    });

    it('string.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({stringProp: PropTypes.string.custom(customFn)});
      const mess1 = check({stringProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({stringProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({stringProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({stringProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
    });

    it('string.isRequired.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({stringProp: PropTypes.string.isRequired.custom(customFn)});
      const mess1 = check({stringProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({stringProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({stringProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({stringProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
    });

    it('string.isRequiredOrNull.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({stringProp: PropTypes.string.isRequiredOrNull.custom(customFn)});
      const mess1 = check({stringProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({stringProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({stringProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({stringProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
    });

    it('string.custom.isRequired', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({stringProp: PropTypes.string.custom(customFn).isRequired});
      const mess1 = check({stringProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({stringProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({stringProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({stringProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
    });

    it('string.custom.isRequiredOrNull', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'err' ? 'custom err mess' : null);
      };
      const check = checkProps({stringProp: PropTypes.string.custom(customFn).isRequiredOrNull});
      const mess1 = check({stringProp: 'any normal string'});
      assert.strictEqual(mess1, null);
      const mess2 = check({stringProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({stringProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({stringProp: 'err'});
      assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
    });

  });

  describe('bool', () => {

    it('bool', () => {
      const check = checkProps({boolProp: PropTypes.bool});
      const mess0 = check({boolProp: true});
      assert.strictEqual(mess0, null);
      const mess1 = check({boolProp: false});
      assert.strictEqual(mess1, null);
      const mess2 = check({});
      assert.strictEqual(mess2, null);
      const mess3 = check({boolProp: undefined});
      assert.strictEqual(mess3, null);
      const mess4 = check({boolProp: null});
      assert.strictEqual(mess4, null);
      const mess5 = check({boolProp: 23});
      assert.strictEqual(mess5, 'PropTypes validation error at [boolProp]: Prop must be a bool when included');
    });

    it('bool.isRequired', () => {
      const check = checkProps({boolProp: PropTypes.bool.isRequired});
      const mess0 = check({boolProp: true});
      assert.strictEqual(mess0, null);
      const mess1 = check({boolProp: false});
      assert.strictEqual(mess1, null);
      const mess2 = check({});
      assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({boolProp: undefined});
      assert.strictEqual(mess3, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({boolProp: null});
      assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
      const mess5 = check({boolProp: 23});
      assert.strictEqual(mess5, 'PropTypes validation error at [boolProp]: Prop must be a bool when included');
    });

    it('bool.isRequiredOrNull', () => {
      const check = checkProps({boolProp: PropTypes.bool.isRequiredOrNull});
      const mess0 = check({boolProp: true});
      assert.strictEqual(mess0, null);
      const mess1 = check({boolProp: false});
      assert.strictEqual(mess1, null);
      const mess2 = check({});
      assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({boolProp: undefined});
      assert.strictEqual(mess3, 'PropTypes validation error at [boolProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess4 = check({boolProp: null});
      assert.strictEqual(mess4, null);
      const mess5 = check({boolProp: 23});
      assert.strictEqual(mess5, 'PropTypes validation error at [boolProp]: Prop must be a bool when included');
    });

    it('bool.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === false ? 'custom err mess' : null);
      };
      const check = checkProps({boolProp: PropTypes.bool.custom(customFn)});
      const mess1 = check({boolProp: true});
      assert.strictEqual(mess1, null);
      const mess2 = check({boolProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({boolProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({boolProp: false});
      assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
    });

    it('bool.isRequired.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === false ? 'custom err mess' : null);
      };
      const check = checkProps({boolProp: PropTypes.bool.isRequired.custom(customFn)});
      const mess1 = check({boolProp: true});
      assert.strictEqual(mess1, null);
      const mess2 = check({boolProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({boolProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({boolProp: false});
      assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
    });

    it('bool.isRequiredOrNull.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === false ? 'custom err mess' : null);
      };
      const check = checkProps({boolProp: PropTypes.bool.isRequiredOrNull.custom(customFn)});
      const mess1 = check({boolProp: true});
      assert.strictEqual(mess1, null);
      const mess2 = check({boolProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({boolProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({boolProp: false});
      assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
    });

    it('bool.custom.isRequired', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === false ? 'custom err mess' : null);
      };
      const check = checkProps({boolProp: PropTypes.bool.custom(customFn).isRequired});
      const mess1 = check({boolProp: true});
      assert.strictEqual(mess1, null);
      const mess2 = check({boolProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({boolProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [boolProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({boolProp: false});
      assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
    });

    it('bool.custom.isRequiredOrNull', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === false ? 'custom err mess' : null);
      };
      const check = checkProps({boolProp: PropTypes.bool.custom(customFn).isRequiredOrNull});
      const mess1 = check({boolProp: true});
      assert.strictEqual(mess1, null);
      const mess2 = check({boolProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [boolProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({boolProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({boolProp: false});
      assert.strictEqual(mess4, 'PropTypes validation error at [boolProp]: custom err mess');
    });

  });

  describe('number', () => {

    it('number', () => {
      const check = checkProps({numberProp: PropTypes.number});
      const checkees = [
        {props: {numberProp: 'asdf'}, mess: 'PropTypes validation error at [numberProp]: Prop must be a number when included'},
        {props: {numberProp: 0}, mess: null},
        {props: {numberProp: 23}, mess: null},
        {props: {numberProp: Number(23)}, mess: null},
        {props: {numberProp: new Number(23)}, mess: null},
        {props: {numberProp: new Number('23')}, mess: null},
        {props: {}, mess: null},
        {props: {numberProp: undefined}, mess: null},
        {props: {numberProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('number.isRequired', () => {
      const check = checkProps({numberProp: PropTypes.number.isRequired});
      const checkees = [
        {props: {numberProp: 'asdf'}, mess: 'PropTypes validation error at [numberProp]: Prop must be a number when included'},
        {props: {numberProp: 0}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {numberProp: undefined}, mess: 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {numberProp: null}, mess: 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('number.isRequiredOrNull', () => {
      const check = checkProps({numberProp: PropTypes.number.isRequiredOrNull});
      const checkees = [
        {props: {numberProp: 'asdf'}, mess: 'PropTypes validation error at [numberProp]: Prop must be a number when included'},
        {props: {numberProp: 0}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [numberProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {numberProp: undefined}, mess: 'PropTypes validation error at [numberProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {numberProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('number.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 43 ? 'custom err mess' : null);
      };
      const check = checkProps({numberProp: PropTypes.number.custom(customFn)});
      const mess1 = check({numberProp: 0});
      assert.strictEqual(mess1, null);
      const mess2 = check({numberProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({numberProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({numberProp: 43});
      assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
    });

    it('number.isRequired.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 43 ? 'custom err mess' : null);
      };
      const check = checkProps({numberProp: PropTypes.number.isRequired.custom(customFn)});
      const mess1 = check({numberProp: 0});
      assert.strictEqual(mess1, null);
      const mess2 = check({numberProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({numberProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({numberProp: 43});
      assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
    });

    it('number.isRequiredOrNull.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 43 ? 'custom err mess' : null);
      };
      const check = checkProps({numberProp: PropTypes.number.isRequiredOrNull.custom(customFn)});
      const mess1 = check({numberProp: 0});
      assert.strictEqual(mess1, null);
      const mess2 = check({numberProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [numberProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({numberProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({numberProp: 43});
      assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
    });

    it('number.custom.isRequired', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 43 ? 'custom err mess' : null);
      };
      const check = checkProps({numberProp: PropTypes.number.custom(customFn).isRequired});
      const mess1 = check({numberProp: 0});
      assert.strictEqual(mess1, null);
      const mess2 = check({numberProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({numberProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({numberProp: 43});
      assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
    });

    it('number.custom.isRequiredOrNull', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 43 ? 'custom err mess' : null);
      };
      const check = checkProps({numberProp: PropTypes.number.custom(customFn).isRequiredOrNull});
      const mess1 = check({numberProp: 0});
      assert.strictEqual(mess1, null);
      const mess2 = check({numberProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [numberProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({numberProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({numberProp: 43});
      assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
    });

  });

  describe('symbol', () => {

    it('symbol', () => {
      const check = checkProps({symbolProp: PropTypes.symbol});
      const checkees = [
        {props: {symbolProp: 'asdf'}, mess: 'PropTypes validation error at [symbolProp]: Prop must be a symbol when included'},
        {props: {symbolProp: Symbol()}, mess: null},
        {props: {symbolProp: Symbol('foo')}, mess: null},
        {props: {}, mess: null},
        {props: {symbolProp: undefined}, mess: null},
        {props: {symbolProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('symbol.isRequired', () => {
      const check = checkProps({symbolProp: PropTypes.symbol.isRequired});
      const checkees = [
        {props: {symbolProp: 'asdf'}, mess: 'PropTypes validation error at [symbolProp]: Prop must be a symbol when included'},
        {props: {symbolProp: Symbol()}, mess: null},
        {props: {symbolProp: Symbol('foo')}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {symbolProp: undefined}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {symbolProp: null}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('symbol.isRequiredOrNull', () => {
      const check = checkProps({symbolProp: PropTypes.symbol.isRequiredOrNull});
      const checkees = [
        {props: {symbolProp: 'asdf'}, mess: 'PropTypes validation error at [symbolProp]: Prop must be a symbol when included'},
        {props: {symbolProp: Symbol()}, mess: null},
        {props: {symbolProp: Symbol('foo')}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {symbolProp: undefined}, mess: 'PropTypes validation error at [symbolProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {symbolProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('symbol.custom', () => {
      const errSymbol = Symbol('err');
      const customFn = ({prop, propName, props}) => {
        return (prop === errSymbol ? 'custom err mess' : null);
      };
      const check = checkProps({symbolProp: PropTypes.symbol.custom(customFn)});
      const mess1 = check({symbolProp: Symbol()});
      assert.strictEqual(mess1, null);
      const mess2 = check({symbolProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({symbolProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({symbolProp: errSymbol});
      assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
    });

    it('symbol.isRequired.custom', () => {
      const errSymbol = Symbol('err');
      const customFn = ({prop, propName, props}) => {
        return (prop === errSymbol ? 'custom err mess' : null);
      };
      const check = checkProps({symbolProp: PropTypes.symbol.isRequired.custom(customFn)});
      const mess1 = check({symbolProp: Symbol()});
      assert.strictEqual(mess1, null);
      const mess2 = check({symbolProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({symbolProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({symbolProp: errSymbol});
      assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
    });

    it('symbol.isRequiredOrNull.custom', () => {
      const errSymbol = Symbol('err');
      const customFn = ({prop, propName, props}) => {
        return (prop === errSymbol ? 'custom err mess' : null);
      };
      const check = checkProps({symbolProp: PropTypes.symbol.isRequiredOrNull.custom(customFn)});
      const mess1 = check({symbolProp: Symbol()});
      assert.strictEqual(mess1, null);
      const mess2 = check({symbolProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [symbolProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({symbolProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({symbolProp: errSymbol});
      assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
    });

    it('symbol.custom.isRequired', () => {
      const errSymbol = Symbol('err');
      const customFn = ({prop, propName, props}) => {
        return (prop === errSymbol ? 'custom err mess' : null);
      };
      const check = checkProps({symbolProp: PropTypes.symbol.custom(customFn).isRequired});
      const mess1 = check({symbolProp: Symbol()});
      assert.strictEqual(mess1, null);
      const mess2 = check({symbolProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({symbolProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [symbolProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({symbolProp: errSymbol});
      assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
    });

    it('symbol.custom.isRequiredOrNull', () => {
      const errSymbol = Symbol('err');
      const customFn = ({prop, propName, props}) => {
        return (prop === errSymbol ? 'custom err mess' : null);
      };
      const check = checkProps({symbolProp: PropTypes.symbol.custom(customFn).isRequiredOrNull});
      const mess1 = check({symbolProp: Symbol()});
      assert.strictEqual(mess1, null);
      const mess2 = check({symbolProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [symbolProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({symbolProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({symbolProp: errSymbol});
      assert.strictEqual(mess4, 'PropTypes validation error at [symbolProp]: custom err mess');
    });

  });

  describe('func', () => {

    it('func', () => {
      const check = checkProps({funcProp: PropTypes.func});
      const checkees = [
        {props: {funcProp: 'asdf'}, mess: 'PropTypes validation error at [funcProp]: Prop must be a function when included'},
        {props: {funcProp: () => 0}, mess: null},
        {props: {funcProp: async () => 0}, mess: null},
        {props: {}, mess: null},
        {props: {funcProp: undefined}, mess: null},
        {props: {funcProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('func.isRequired', () => {
      const check = checkProps({funcProp: PropTypes.func.isRequired});
      const checkees = [
        {props: {funcProp: 'asdf'}, mess: 'PropTypes validation error at [funcProp]: Prop must be a function when included'},
        {props: {funcProp: () => 0}, mess: null},
        {props: {funcProp: async () => 0}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {funcProp: undefined}, mess: 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {funcProp: null}, mess: 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('func.isRequiredOrNull', () => {
      const check = checkProps({funcProp: PropTypes.func.isRequiredOrNull});
      const checkees = [
        {props: {funcProp: 'asdf'}, mess: 'PropTypes validation error at [funcProp]: Prop must be a function when included'},
        {props: {funcProp: () => 0}, mess: null},
        {props: {funcProp: async () => 0}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [funcProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {funcProp: undefined}, mess: 'PropTypes validation error at [funcProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {funcProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('func.custom', () => {
      const errFunc = () => 0;
      const customFn = ({prop, propName, props}) => {
        return (prop === errFunc ? 'custom err mess' : null);
      };
      const check = checkProps({funcProp: PropTypes.func.custom(customFn)});
      const mess1 = check({funcProp: () => true});
      assert.strictEqual(mess1, null);
      const mess2 = check({funcProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({funcProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({funcProp: errFunc});
      assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
    });

    it('func.isRequired.custom', () => {
      const errFunc = () => 0;
      const customFn = ({prop, propName, props}) => {
        return (prop === errFunc ? 'custom err mess' : null);
      };
      const check = checkProps({funcProp: PropTypes.func.isRequired.custom(customFn)});
      const mess1 = check({funcProp: () => true});
      assert.strictEqual(mess1, null);
      const mess2 = check({funcProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({funcProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({funcProp: errFunc});
      assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
    });

    it('func.isRequiredOrNull.custom', () => {
      const errFunc = () => 0;
      const customFn = ({prop, propName, props}) => {
        return (prop === errFunc ? 'custom err mess' : null);
      };
      const check = checkProps({funcProp: PropTypes.func.isRequiredOrNull.custom(customFn)});
      const mess1 = check({funcProp: () => true});
      assert.strictEqual(mess1, null);
      const mess2 = check({funcProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [funcProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({funcProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({funcProp: errFunc});
      assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
    });

    it('func.custom.isRequired', () => {
      const errFunc = () => 0;
      const customFn = ({prop, propName, props}) => {
        return (prop === errFunc ? 'custom err mess' : null);
      };
      const check = checkProps({funcProp: PropTypes.func.custom(customFn).isRequired});
      const mess1 = check({funcProp: () => true});
      assert.strictEqual(mess1, null);
      const mess2 = check({funcProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({funcProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [funcProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({funcProp: errFunc});
      assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
    });

    it('func.custom.isRequiredOrNull', () => {
      const errFunc = () => 0;
      const customFn = ({prop, propName, props}) => {
        return (prop === errFunc ? 'custom err mess' : null);
      };
      const check = checkProps({funcProp: PropTypes.func.custom(customFn).isRequiredOrNull});
      const mess1 = check({funcProp: () => true});
      assert.strictEqual(mess1, null);
      const mess2 = check({funcProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [funcProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({funcProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({funcProp: errFunc});
      assert.strictEqual(mess4, 'PropTypes validation error at [funcProp]: custom err mess');
    });

  });

  describe('object', () => {

    it('object', () => {
      const check = checkProps({objectProp: PropTypes.object});
      const checkees = [
        {props: {objectProp: 'asdf'}, mess: 'PropTypes validation error at [objectProp]: Prop must be an object when included'},
        {props: {objectProp: {}}, mess: null},
        {props: {objectProp: Object()}, mess: null},
        {props: {objectProp: []}, mess: null},
        {props: {objectProp: () => 0}, mess: null},
        {props: {objectProp: async () => 0}, mess: null},
        {props: {}, mess: null},
        {props: {objectProp: undefined}, mess: null},
        {props: {objectProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('object.isRequired', () => {
      const check = checkProps({objectProp: PropTypes.object.isRequired});
      const checkees = [
        {props: {objectProp: 'asdf'}, mess: 'PropTypes validation error at [objectProp]: Prop must be an object when included'},
        {props: {objectProp: {}}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {objectProp: undefined}, mess: 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {objectProp: null}, mess: 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('object.isRequiredOrNull', () => {
      const check = checkProps({objectProp: PropTypes.object.isRequiredOrNull});
      const checkees = [
        {props: {objectProp: 'asdf'}, mess: 'PropTypes validation error at [objectProp]: Prop must be an object when included'},
        {props: {objectProp: {}}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [objectProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {objectProp: undefined}, mess: 'PropTypes validation error at [objectProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {objectProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('object.custom', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({objectProp: PropTypes.object.custom(customFn)});
      const mess1 = check({objectProp: {some: 'object'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({objectProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({objectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
    });

    it('object.isRequired.custom', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({objectProp: PropTypes.object.isRequired.custom(customFn)});
      const mess1 = check({objectProp: {some: 'object'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({objectProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({objectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
    });

    it('object.isRequiredOrNull.custom', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({objectProp: PropTypes.object.isRequiredOrNull.custom(customFn)});
      const mess1 = check({objectProp: {some: 'object'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [objectProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({objectProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({objectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
    });

    it('object.custom.isRequired', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({objectProp: PropTypes.object.custom(customFn).isRequired});
      const mess1 = check({objectProp: {some: 'object'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({objectProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({objectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
    });

    it('object.custom.isRequiredOrNull', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({objectProp: PropTypes.object.custom(customFn).isRequiredOrNull});
      const mess1 = check({objectProp: {some: 'object'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [objectProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({objectProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({objectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
    });

  });

  describe('plainObject', () => {

    it('plainObject', () => {
      const check = checkProps({plainObjectProp: PropTypes.plainObject});
      const checkees = [
        {props: {plainObjectProp: 'asdf'}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
        {props: {plainObjectProp: {}}, mess: null},
        {props: {plainObjectProp: Object()}, mess: null},
        {props: {plainObjectProp: []}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
        {props: {plainObjectProp: () => 0}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
        {props: {plainObjectProp: async () => 0}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
        {props: {}, mess: null},
        {props: {plainObjectProp: undefined}, mess: null},
        {props: {plainObjectProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('plainObject.isRequired', () => {
      const check = checkProps({plainObjectProp: PropTypes.plainObject.isRequired});
      const checkees = [
        {props: {plainObjectProp: 'asdf'}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
        {props: {plainObjectProp: {}}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {plainObjectProp: undefined}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {plainObjectProp: null}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('plainObject.isRequiredOrNull', () => {
      const check = checkProps({plainObjectProp: PropTypes.plainObject.isRequiredOrNull});
      const checkees = [
        {props: {plainObjectProp: 'asdf'}, mess: 'PropTypes validation error at [plainObjectProp]: Prop must be a plain object when included'},
        {props: {plainObjectProp: {}}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {plainObjectProp: undefined}, mess: 'PropTypes validation error at [plainObjectProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {plainObjectProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('plainObject.custom', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({plainObjectProp: PropTypes.plainObject.custom(customFn)});
      const mess1 = check({plainObjectProp: {some: 'plainObject'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({plainObjectProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({plainObjectProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({plainObjectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
    });

    it('plainObject.isRequired.custom', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({plainObjectProp: PropTypes.plainObject.isRequired.custom(customFn)});
      const mess1 = check({plainObjectProp: {some: 'plainObject'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({plainObjectProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({plainObjectProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({plainObjectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
    });

    it('plainObject.isRequiredOrNull.custom', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({plainObjectProp: PropTypes.plainObject.isRequiredOrNull.custom(customFn)});
      const mess1 = check({plainObjectProp: {some: 'plainObject'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({plainObjectProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [plainObjectProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({plainObjectProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({plainObjectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
    });

    it('plainObject.custom.isRequired', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({plainObjectProp: PropTypes.plainObject.custom(customFn).isRequired});
      const mess1 = check({plainObjectProp: {some: 'plainObject'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({plainObjectProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({plainObjectProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [plainObjectProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({plainObjectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
    });

    it('plainObject.custom.isRequiredOrNull', () => {
      const errObj = {derp: 'merp'};
      const customFn = ({prop, propName, props}) => {
        return (prop === errObj ? 'custom err mess' : null);
      };
      const check = checkProps({plainObjectProp: PropTypes.plainObject.custom(customFn).isRequiredOrNull});
      const mess1 = check({plainObjectProp: {some: 'plainObject'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({plainObjectProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [plainObjectProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({plainObjectProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({plainObjectProp: errObj});
      assert.strictEqual(mess4, 'PropTypes validation error at [plainObjectProp]: custom err mess');
    });

  });

  describe('array', () => {

    it('array', () => {
      const check = checkProps({arrayProp: PropTypes.array});
      const checkees = [
        {props: {arrayProp: 'asdf'}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
        {props: {arrayProp: {}}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
        {props: {arrayProp: Object()}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
        {props: {arrayProp: []}, mess: null},
        {props: {arrayProp: Array()}, mess: null},
        {props: {}, mess: null},
        {props: {arrayProp: undefined}, mess: null},
        {props: {arrayProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('array.isRequired', () => {
      const check = checkProps({arrayProp: PropTypes.array.isRequired});
      const checkees = [
        {props: {arrayProp: 'asdf'}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
        {props: {arrayProp: []}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {arrayProp: undefined}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {arrayProp: null}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('array.isRequiredOrNull', () => {
      const check = checkProps({arrayProp: PropTypes.array.isRequiredOrNull});
      const checkees = [
        {props: {arrayProp: 'asdf'}, mess: 'PropTypes validation error at [arrayProp]: Prop must be an array when included'},
        {props: {arrayProp: []}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {arrayProp: undefined}, mess: 'PropTypes validation error at [arrayProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {arrayProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('array.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
      };
      const check = checkProps({arrayProp: PropTypes.array.custom(customFn)});
      const mess1 = check({arrayProp: []});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({arrayProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
    });

    it('array.isRequired.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
      };
      const check = checkProps({arrayProp: PropTypes.array.isRequired.custom(customFn)});
      const mess1 = check({arrayProp: []});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({arrayProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
    });

    it('array.isRequiredOrNull.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
      };
      const check = checkProps({arrayProp: PropTypes.array.isRequiredOrNull.custom(customFn)});
      const mess1 = check({arrayProp: []});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [arrayProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({arrayProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
    });

    it('array.custom.isRequired', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
      };
      const check = checkProps({arrayProp: PropTypes.array.custom(customFn).isRequired});
      const mess1 = check({arrayProp: []});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({arrayProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [arrayProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
    });

    it('array.custom.isRequiredOrNull', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.get(prop, 'length') === 5 ? 'custom err mess' : null);
      };
      const check = checkProps({arrayProp: PropTypes.array.custom(customFn).isRequiredOrNull});
      const mess1 = check({arrayProp: []});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [arrayProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({arrayProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({arrayProp: [1, 2, 3, 4, 5]});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayProp]: custom err mess');
    });

  });

  describe('oneOf (enum)', () => {

    it('some valid propEnums', () => {
      const valids = [
        [],
        ['a'],
        ['slksdf', 23, {}],
        [true, false],
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps({oneOfProp: PropTypes.oneOf(valid)}));
      });
    });

    it('some invalid propEnums', () => {
      const derp = {};
      const invalids = [
        {propEnum: 'notAnArray', mess: /propEnum must be an array/},
        {propEnum: [1, 1, 2], mess: /propEnum must contain unique values/},
        {propEnum: [derp, derp], mess: /propEnum must contain unique values/},
        {propEnum: ['one', null, 'two'], mess: /propEnum cannot contain undefined or null/},
        {propEnum: ['one', undefined, 'two'], mess: /propEnum cannot contain undefined or null/},
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({oneOfProp: PropTypes.oneOf(invalid.propEnum)}), invalid.mess);
      });
    });

    it('oneOf - default', () => {
      const derp = {};
      const propEnum = ['berp', derp, 'lerp'];
      const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum)});
      const checkees = [
        {props: {oneOfProp: 'asdf'}, mess: 'PropTypes validation error at [oneOfProp]: Prop must be contained in enum: [berp, [object Object], lerp]'},
        {props: {oneOfProp: 'berp'}, mess: null},
        {props: {oneOfProp: derp}, mess: null},
        {props: {oneOfProp: String('berp')}, mess: null},
        {props: {}, mess: null},
        {props: {oneOfProp: undefined}, mess: null},
        {props: {oneOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('oneOf - when enum has more than 3 the enum list gets cropped', () => {
      const derp = {};
      const propEnum = ['berp', derp, 'lerp', 'snerp'];
      const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum)});
      const checkee = {
        props: {oneOfProp: 'asdf'},
        mess: 'PropTypes validation error at [oneOfProp]: Prop must be contained in enum: [berp, [object Object], lerp, ...]',
      };
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });

    it('oneOf.isRequired', () => {
      const derp = {};
      const propEnum = ['berp', derp, 'lerp'];
      const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).isRequired});
      const checkees = [
        {props: {oneOfProp: 'asdf'}, mess: 'PropTypes validation error at [oneOfProp]: Prop must be contained in enum: [berp, [object Object], lerp]'},
        {props: {oneOfProp: 'berp'}, mess: null},
        {props: {oneOfProp: derp}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {oneOfProp: undefined}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {oneOfProp: null}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('oneOf.isRequiredOrNull', () => {
      const derp = {};
      const propEnum = ['berp', derp, 'lerp'];
      const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).isRequiredOrNull});
      const checkees = [
        {props: {oneOfProp: 'asdf'}, mess: 'PropTypes validation error at [oneOfProp]: Prop must be contained in enum: [berp, [object Object], lerp]'},
        {props: {oneOfProp: 'berp'}, mess: null},
        {props: {oneOfProp: derp}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {oneOfProp: undefined}, mess: 'PropTypes validation error at [oneOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {oneOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('oneOf.custom', () => {
      const derp = {};
      const propEnum = ['berp', derp, 'lerp'];
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).custom(customFn)});
      const mess1 = check({oneOfProp: 'berp'});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({oneOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({oneOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
    });

    it('oneOf.isRequired.custom', () => {
      const derp = {};
      const propEnum = ['berp', derp, 'lerp'];
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).isRequired.custom(customFn)});
      const mess1 = check({oneOfProp: 'berp'});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({oneOfProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({oneOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
    });

    it('oneOf.isRequiredOrNull.custom', () => {
      const derp = {};
      const propEnum = ['berp', derp, 'lerp'];
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).isRequiredOrNull.custom(customFn)});
      const mess1 = check({oneOfProp: 'berp'});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [oneOfProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({oneOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({oneOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
    });

    it('oneOf.custom.isRequired', () => {
      const derp = {};
      const propEnum = ['berp', derp, 'lerp'];
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).custom(customFn).isRequired});
      const mess1 = check({oneOfProp: 'berp'});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({oneOfProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [oneOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({oneOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
    });

    it('oneOf.custom.isRequiredOrNull', () => {
      const derp = {};
      const propEnum = ['berp', derp, 'lerp'];
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfProp: PropTypes.oneOf(propEnum).custom(customFn).isRequiredOrNull});
      const mess1 = check({oneOfProp: 'berp'});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [oneOfProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({oneOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({oneOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfProp]: custom err mess');
    });

  });

  describe('instanceOf (class)', () => {

    it('some valid propClasses', () => {
      const Derp = function () {
        this.derp = 'derp';
      };
      const valids = [
        Date,
        Derp,
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps({instanceOfProp: PropTypes.instanceOf(valid)}));
      });
    });

    it('some invalid propClasses', () => {
      const derp = {};
      const invalids = [
        {propClass: {}, mess: /The Class must be a callable/},
        {propClass: () => 0, mess: /The Class must be a callable/},
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({instanceOfProp: PropTypes.instanceOf(invalid.propClass)}), invalid.mess);
      });
    });

    it('instanceOf - default', () => {
      const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date)});
      const checkees = [
        {props: {instanceOfProp: 'asdf'}, mess: 'PropTypes validation error at [instanceOfProp]: Prop must be an instance of class: function Date() { [native code] }'},
        {props: {instanceOfProp: new Date()}, mess: null},
        {props: {}, mess: null},
        {props: {instanceOfProp: undefined}, mess: null},
        {props: {instanceOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('instanceOf.isRequired - default', () => {
      const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).isRequired});
      const checkees = [
        {props: {instanceOfProp: 'asdf'}, mess: 'PropTypes validation error at [instanceOfProp]: Prop must be an instance of class: function Date() { [native code] }'},
        {props: {instanceOfProp: new Date()}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {instanceOfProp: undefined}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {instanceOfProp: null}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('instanceOf.isRequiredOrNull - default', () => {
      const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).isRequiredOrNull});
      const checkees = [
        {props: {instanceOfProp: 'asdf'}, mess: 'PropTypes validation error at [instanceOfProp]: Prop must be an instance of class: function Date() { [native code] }'},
        {props: {instanceOfProp: new Date()}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {instanceOfProp: undefined}, mess: 'PropTypes validation error at [instanceOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {instanceOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('instanceOf.custom', () => {
      const derp = new Date();
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).custom(customFn)});
      const mess1 = check({instanceOfProp: new Date()});
      assert.strictEqual(mess1, null);
      const mess2 = check({instanceOfProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({instanceOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({instanceOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
    });

    it('instanceOf.isRequired.custom', () => {
      const derp = new Date();
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).isRequired.custom(customFn)});
      const mess1 = check({instanceOfProp: new Date()});
      assert.strictEqual(mess1, null);
      const mess2 = check({instanceOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({instanceOfProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({instanceOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
    });

    it('instanceOf.isRequiredOrNull.custom', () => {
      const derp = new Date();
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).isRequiredOrNull.custom(customFn)});
      const mess1 = check({instanceOfProp: new Date()});
      assert.strictEqual(mess1, null);
      const mess2 = check({instanceOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [instanceOfProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({instanceOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({instanceOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
    });

    it('instanceOf.custom.isRequired', () => {
      const derp = new Date();
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).custom(customFn).isRequired});
      const mess1 = check({instanceOfProp: new Date()});
      assert.strictEqual(mess1, null);
      const mess2 = check({instanceOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({instanceOfProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [instanceOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({instanceOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
    });

    it('instanceOf.custom.isRequiredOrNull', () => {
      const derp = new Date();
      const customFn = ({prop, propName, props}) => {
        return (prop === derp ? 'custom err mess' : null);
      };
      const check = checkProps({instanceOfProp: PropTypes.instanceOf(Date).custom(customFn).isRequiredOrNull});
      const mess1 = check({instanceOfProp: new Date()});
      assert.strictEqual(mess1, null);
      const mess2 = check({instanceOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [instanceOfProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({instanceOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({instanceOfProp: derp});
      assert.strictEqual(mess4, 'PropTypes validation error at [instanceOfProp]: custom err mess');
    });

  });

  describe('arrayOf (propType)', () => {

    it('some valid propTypes', () => {
      const valids = [
        PropTypes.any,
        PropTypes.string,
        PropTypes.number.isRequired,
        PropTypes.instanceOf(Date),
        PropTypes.arrayOf(PropTypes.any.isRequired),
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps({arrayOfProp: PropTypes.arrayOf(valid)}));
      });
    });

    it('some invalid propTypes', () => {
      const derp = {};
      const invalids = [
        {propType: {}, mess: /invalid propType function for {propName: propType}/},
        {propType: () => 0, mess: /invalid propType function for {propName: propType}/},
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({arrayOfProp: PropTypes.arrayOf(invalid.propType)}), invalid.mess);
      });
    });

    it('arrayOf (PropTypes.string) - default', () => {
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string)});
      const checkees = [
        {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
        {props: {arrayOfProp: []}, mess: null},
        {props: {arrayOfProp: [null]}, mess: null},
        {props: {arrayOfProp: [undefined]}, mess: null},
        {props: {arrayOfProp: [,]}, mess: null},
        {props: {arrayOfProp: ['', 'asdf', '23', undefined, '']}, mess: null},
        {props: {}, mess: null},
        {props: {arrayOfProp: undefined}, mess: null},
        {props: {arrayOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('arrayOf (PropTypes.string.isRequired) - default', () => {
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string.isRequired)});
      const checkees = [
        {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
        {props: {arrayOfProp: []}, mess: null},
        {props: {arrayOfProp: [null]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
        {props: {arrayOfProp: [undefined]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
        {props: {arrayOfProp: [,]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
        {props: {arrayOfProp: ['', 'asdf', '23', undefined, '']}, mess: 'PropTypes validation error at [arrayOfProp, 3]: Prop isRequired (cannot be null or undefined)'},
        {props: {}, mess: null},
        {props: {arrayOfProp: undefined}, mess: null},
        {props: {arrayOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('arrayOf (PropTypes.string.isRequiredOrNull) - default', () => {
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string.isRequiredOrNull)});
      const checkees = [
        {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
        {props: {arrayOfProp: []}, mess: null},
        {props: {arrayOfProp: [null]}, mess: null},
        {props: {arrayOfProp: [undefined]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {arrayOfProp: [,]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {arrayOfProp: ['', 'asdf', '23', undefined, '']}, mess: 'PropTypes validation error at [arrayOfProp, 3]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {}, mess: null},
        {props: {arrayOfProp: undefined}, mess: null},
        {props: {arrayOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('arrayOf.isRequired (any) - default', () => {
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.any).isRequired});
      const checkees = [
        {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
        {props: {arrayOfProp: []}, mess: null},
        {props: {arrayOfProp: {}}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
        {props: {}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {arrayOfProp: undefined}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {arrayOfProp: null}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('arrayOf.isRequiredOrNull (any) - default', () => {
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.any).isRequiredOrNull});
      const checkees = [
        {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
        {props: {arrayOfProp: []}, mess: null},
        {props: {arrayOfProp: {}}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
        {props: {}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {arrayOfProp: undefined}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {arrayOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('arrayOf.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).custom(customFn)});
      const mess1 = check({arrayOfProp: ['xxx']});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayOfProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({arrayOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({arrayOfProp: ['derp']});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
    });

    it('arrayOf.isRequired.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).isRequired.custom(customFn)});
      const mess1 = check({arrayOfProp: ['xxx']});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({arrayOfProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({arrayOfProp: ['derp']});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
    });

    it('arrayOf.isRequiredOrNull.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).isRequiredOrNull.custom(customFn)});
      const mess1 = check({arrayOfProp: ['xxx']});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [arrayOfProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({arrayOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({arrayOfProp: ['derp']});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
    });

    it('arrayOf.custom.isRequired', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).custom(customFn).isRequired});
      const mess1 = check({arrayOfProp: ['xxx']});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({arrayOfProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({arrayOfProp: ['derp']});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
    });

    it('arrayOf.custom.isRequiredOrNull', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).custom(customFn).isRequiredOrNull});
      const mess1 = check({arrayOfProp: ['xxx']});
      assert.strictEqual(mess1, null);
      const mess2 = check({arrayOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [arrayOfProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({arrayOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({arrayOfProp: ['derp']});
      assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
    });

  });

  describe('objectOf (propType)', () => {

    it('some valid propTypes', () => {
      const valids = [
        PropTypes.any,
        PropTypes.string,
        PropTypes.number.isRequired,
        PropTypes.instanceOf(Date),
        PropTypes.objectOf(PropTypes.any.isRequired),
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps({objectOfProp: PropTypes.objectOf(valid)}));
      });
    });

    it('some invalid propTypes', () => {
      const derp = {};
      const invalids = [
        {propType: {}, mess: /invalid propType function for {propName: propType}/},
        {propType: () => 0, mess: /invalid propType function for {propName: propType}/},
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({objectOfProp: PropTypes.objectOf(invalid.propType)}), invalid.mess);
      });
    });

    it('objectOf (PropTypes.string) - default', () => {
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string)});
      const checkees = [
        {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
        {props: {objectOfProp: () => {}}, mess: null},
        {props: {objectOfProp: new Date()}, mess: null},
        {props: {objectOfProp: []}, mess: null},
        {props: {objectOfProp: [null]}, mess: null},
        {props: {objectOfProp: [undefined]}, mess: null},
        {props: {objectOfProp: [,]}, mess: null},
        {props: {objectOfProp: ['', 'asdf', '23', undefined, '']}, mess: null},
        {props: {objectOfProp: {}}, mess: null},
        {props: {objectOfProp: {a: null}}, mess: null},
        {props: {objectOfProp: {a: undefined}}, mess: null},
        {props: {}, mess: null},
        {props: {objectOfProp: undefined}, mess: null},
        {props: {objectOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('objectOf (PropTypes.string.isRequired) - default', () => {
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string.isRequired)});
      const checkees = [
        {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
        {props: {objectOfProp: () => {}}, mess: null},
        {props: {objectOfProp: new Date()}, mess: null},
        {props: {objectOfProp: []}, mess: null},
        {props: {objectOfProp: [null]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
        {props: {objectOfProp: [undefined]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
        {props: {objectOfProp: [,]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
        {props: {objectOfProp: ['', 'asdf', '23', undefined, '']}, mess: 'PropTypes validation error at [objectOfProp, 3]: Prop isRequired (cannot be null or undefined)'},
        {props: {objectOfProp: {}}, mess: null},
        {props: {objectOfProp: {a: null}}, mess: 'PropTypes validation error at [objectOfProp, a]: Prop isRequired (cannot be null or undefined)'},
        {props: {objectOfProp: {a: undefined}}, mess: 'PropTypes validation error at [objectOfProp, a]: Prop isRequired (cannot be null or undefined)'},
        {props: {}, mess: null},
        {props: {objectOfProp: undefined}, mess: null},
        {props: {objectOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('objectOf (PropTypes.string.isRequiredOrNull) - default', () => {
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string.isRequiredOrNull)});
      const checkees = [
        {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
        {props: {objectOfProp: () => {}}, mess: null},
        {props: {objectOfProp: new Date()}, mess: null},
        {props: {objectOfProp: []}, mess: null},
        {props: {objectOfProp: [null]}, mess: null},
        {props: {objectOfProp: [undefined]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {objectOfProp: [,]}, mess: 'PropTypes validation error at [objectOfProp, 0]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {objectOfProp: ['', 'asdf', '23', undefined, '']}, mess: 'PropTypes validation error at [objectOfProp, 3]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {objectOfProp: {}}, mess: null},
        {props: {objectOfProp: {a: null}}, mess: null},
        {props: {objectOfProp: {a: undefined}}, mess: 'PropTypes validation error at [objectOfProp, a]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {}, mess: null},
        {props: {objectOfProp: undefined}, mess: null},
        {props: {objectOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('objectOf.isRequired (any) - default', () => {
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.any).isRequired});
      const checkees = [
        {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
        {props: {objectOfProp: []}, mess: null},
        {props: {objectOfProp: {}}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {objectOfProp: undefined}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {objectOfProp: null}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('objectOf.isRequiredOrNull (any) - default', () => {
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.any).isRequiredOrNull});
      const checkees = [
        {props: {objectOfProp: 'asdf'}, mess: 'PropTypes validation error at [objectOfProp]: Prop must be an object (PropTypes.objectOf requirement)'},
        {props: {objectOfProp: []}, mess: null},
        {props: {objectOfProp: {}}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {objectOfProp: undefined}, mess: 'PropTypes validation error at [objectOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {objectOfProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('objectOf.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).custom(customFn)});
      const mess1 = check({objectOfProp: {derp: 'xxx'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectOfProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({objectOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({objectOfProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
    });

    it('objectOf.isRequired.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).isRequired.custom(customFn)});
      const mess1 = check({objectOfProp: {derp: 'xxx'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({objectOfProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({objectOfProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
    });

    it('objectOf.isRequiredOrNull.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).isRequiredOrNull.custom(customFn)});
      const mess1 = check({objectOfProp: {derp: 'xxx'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [objectOfProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({objectOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({objectOfProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
    });

    it('objectOf.custom.isRequired', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).custom(customFn).isRequired});
      const mess1 = check({objectOfProp: {derp: 'xxx'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({objectOfProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [objectOfProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({objectOfProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
    });

    it('objectOf.custom.isRequiredOrNull', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({objectOfProp: PropTypes.objectOf(PropTypes.string).custom(customFn).isRequiredOrNull});
      const mess1 = check({objectOfProp: {derp: 'xxx'}});
      assert.strictEqual(mess1, null);
      const mess2 = check({objectOfProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [objectOfProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({objectOfProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({objectOfProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [objectOfProp]: custom err mess');
    });

  });

  describe('oneOfType (propType)', () => {

    it('some valid propTypes', () => {
      const valid = [
        PropTypes.any,
        PropTypes.string,
        PropTypes.number.isRequired,
        PropTypes.instanceOf(Date),
        PropTypes.oneOfType([PropTypes.any.isRequired]),
      ];
      assert.doesNotThrow(() => checkProps({oneOfTypeProp: PropTypes.oneOfType(valid)}));
    });

    it('some invalid propTypes', () => {
      const derp = {};
      const invalids = [
        {types: null, mess: /the "oneOfType" input must be an array of PropTypes/},
        {types: {}, mess: /the "oneOfType" input must be an array of PropTypes/},
        {types: () => 0, mess: /the "oneOfType" input must be an array of PropTypes/},
        {types: [], mess: /the "oneOfType" input array must have at least one PropTypes item/},
        {types: ['a'], mess: /invalid propType function for {propName: 0}/},
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({oneOfTypeProp: PropTypes.oneOfType(invalid.types)}), invalid.mess);
      });
    });

    it('oneOfType - default', () => {
      const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ])});
      const checkees = [
        {props: {oneOfTypeProp: 'asdf'}, mess: null},
        {props: {oneOfTypeProp: 23}, mess: null},
        {props: {oneOfTypeProp: () => {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
        {props: {oneOfTypeProp: {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
        {props: {}, mess: null},
        {props: {oneOfTypeProp: undefined}, mess: null},
        {props: {oneOfTypeProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('oneOfType - isRequired', () => {
      const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).isRequired});
      const checkees = [
        {props: {oneOfTypeProp: 'asdf'}, mess: null},
        {props: {oneOfTypeProp: 23}, mess: null},
        {props: {oneOfTypeProp: () => {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
        {props: {oneOfTypeProp: {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
        {props: {}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {oneOfTypeProp: undefined}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {oneOfTypeProp: null}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('oneOfType - isRequiredOrNull', () => {
      const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).isRequiredOrNull});
      const checkees = [
        {props: {oneOfTypeProp: 'asdf'}, mess: null},
        {props: {oneOfTypeProp: 23}, mess: null},
        {props: {oneOfTypeProp: () => {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
        {props: {oneOfTypeProp: {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
        {props: {}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {oneOfTypeProp: undefined}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {oneOfTypeProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('oneOfType.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'derp' ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).custom(customFn)});
      const mess1 = check({oneOfTypeProp: 23});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfTypeProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({oneOfTypeProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({oneOfTypeProp: 'derp'});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
    });

    it('oneOfType.isRequired.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'derp' ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).isRequired.custom(customFn)});
      const mess1 = check({oneOfTypeProp: 23});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfTypeProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({oneOfTypeProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({oneOfTypeProp: 'derp'});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
    });

    it('oneOfType.isRequiredOrNull.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'derp' ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).isRequiredOrNull.custom(customFn)});
      const mess1 = check({oneOfTypeProp: 23});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfTypeProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({oneOfTypeProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({oneOfTypeProp: 'derp'});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
    });

    it('oneOfType.custom.isRequired', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'derp' ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).custom(customFn).isRequired});
      const mess1 = check({oneOfTypeProp: 23});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfTypeProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({oneOfTypeProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({oneOfTypeProp: 'derp'});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
    });

    it('oneOfType.custom.isRequiredOrNull', () => {
      const customFn = ({prop, propName, props}) => {
        return (prop === 'derp' ? 'custom err mess' : null);
      };
      const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).custom(customFn).isRequiredOrNull});
      const mess1 = check({oneOfTypeProp: 23});
      assert.strictEqual(mess1, null);
      const mess2 = check({oneOfTypeProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({oneOfTypeProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({oneOfTypeProp: 'derp'});
      assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
    });

  });

  describe('shape (propType)', () => {

    describe('shape options', () => {

      it('some valids', () => {
        const valids = [
          undefined,
          null,
          false,
          0,
          '',
          {},
          {isExact: true},
          {isExact: 'any value really'},
          {isExact: false},
        ];
        _.each(valids, (validShapeOptions) => {
          assert.doesNotThrow(() => checkProps({
            shapeProp: PropTypes.shape({
              arp: PropTypes.any,
            }, validShapeOptions),
          }));
        });
      });

      it('some invalids', () => {
        const invalids = [
          {
            options: {'anythingButIsExact': true, 'two': 2},
            mess: /Invalid shape options \[anythingButIsExact, two\]/,
          },
        ];
        _.each(invalids, invalid => {
          assert.throws(() => checkProps({
            arp: PropTypes.shape({}, invalid.options)
          }), invalid.mess);
        });
      });

      it('props that breaks shape isExact option', () => {
        const invalids = [
          {
            props: {shapeProp: {blue: 1}},
            mess: 'PropTypes validation error at [shapeProp]: PropType.shape with isExact=true has extra (unallowed) props: [blue]',
          },
          {
            props: {shapeProp: {blue: 1, red: 2, green: 3}},
            mess: 'PropTypes validation error at [shapeProp]: PropType.shape with isExact=true has extra (unallowed) props: [blue, red, green]',
          },
          {
            props: {shapeProp: {blue: 1, red: 2, green: 3, purple: 4}},
            mess: 'PropTypes validation error at [shapeProp]: PropType.shape with isExact=true has extra (unallowed) props: [blue, red, green, ...]',
          },
        ];
        _.each(invalids, invalid => {
          const check = checkProps({
            shapeProp: PropTypes.shape({}, {isExact: true}),
          });
          const mess = check(invalid.props);
          assert.strictEqual(mess, invalid.mess);
        });
      });

    });

    it('some valid propTypes', () => {
      const valids = [
        {},
        [],
        () => {}, // is an object..
        {derp: PropTypes.any},
        {derp: PropTypes.string},
        {derp: PropTypes.number.isRequired},
        {derp: PropTypes.instanceOf(Date)},
        {derp: PropTypes.shape({smerp: PropTypes.any.isRequired})},
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps({shapeProp: PropTypes.shape(valid)}));
      });
    });

    it('some invalid propTypes', () => {
      const derp = {};
      const invalids = [
        {shape: null, mess: /the "shape" input must be an object with PropTypes as props/},
        {shape: 23, mess: /the "shape" input must be an object with PropTypes as props/},
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({shapeProp: PropTypes.shape(invalid.types)}), invalid.mess);
      });
    });

    it('shape - default', () => {
      const check = checkProps({shapeProp: PropTypes.shape({
        derp: PropTypes.string,
        smerp: PropTypes.number,
      })});
      const checkees = [
        {props: {shapeProp: {}}, mess: null},
        {props: {shapeProp: {a: 'asdf', b: 23}}, mess: null},
        {props: {}, mess: null},
        {props: {shapeProp: undefined}, mess: null},
        {props: {shapeProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('shape - isRequired', () => {
      const check = checkProps({shapeProp: PropTypes.shape({
        derp: PropTypes.string,
        smerp: PropTypes.number,
      }).isRequired});
      const checkees = [
        {props: {shapeProp: {}}, mess: null},
        {props: {shapeProp: {a: 'asdf', b: 23}}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {shapeProp: undefined}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {shapeProp: null}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('shape - isRequiredOrNull', () => {
      const check = checkProps({shapeProp: PropTypes.shape({
        derp: PropTypes.string,
        smerp: PropTypes.number,
      }).isRequiredOrNull});
      const checkees = [
        {props: {shapeProp: {}}, mess: null},
        {props: {shapeProp: {a: 'asdf', b: 23}}, mess: null},
        {props: {}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {shapeProp: undefined}, mess: 'PropTypes validation error at [shapeProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {shapeProp: null}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('shape.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({shapeProp: PropTypes.shape({
        derp: PropTypes.string,
        smerp: PropTypes.number,
      }).custom(customFn)});
      const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
      assert.strictEqual(mess1, null);
      const mess2 = check({shapeProp: undefined});
      assert.strictEqual(mess2, null);
      const mess3 = check({shapeProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({shapeProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
    });

    it('shape.isRequired.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({shapeProp: PropTypes.shape({
        derp: PropTypes.string,
        smerp: PropTypes.number,
      }).isRequired.custom(customFn)});
      const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
      assert.strictEqual(mess1, null);
      const mess2 = check({shapeProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({shapeProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({shapeProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
    });

    it('shape.isRequiredOrNull.custom', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({shapeProp: PropTypes.shape({
        derp: PropTypes.string,
        smerp: PropTypes.number,
      }).isRequiredOrNull.custom(customFn)});
      const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
      assert.strictEqual(mess1, null);
      const mess2 = check({shapeProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [shapeProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({shapeProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({shapeProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
    });

    it('shape.custom.isRequired', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({shapeProp: PropTypes.shape({
        derp: PropTypes.string,
        smerp: PropTypes.number,
      }).custom(customFn).isRequired});
      const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
      assert.strictEqual(mess1, null);
      const mess2 = check({shapeProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)');
      const mess3 = check({shapeProp: null});
      assert.strictEqual(mess3, 'PropTypes validation error at [shapeProp]: Prop isRequired (cannot be null or undefined)');
      const mess4 = check({shapeProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
    });

    it('shape.custom.isRequiredOrNull', () => {
      const customFn = ({prop, propName, props}) => {
        return (_.includes(prop, 'derp') ? 'custom err mess' : null);
      };
      const check = checkProps({shapeProp: PropTypes.shape({
        derp: PropTypes.string,
        smerp: PropTypes.number,
      }).custom(customFn).isRequiredOrNull});
      const mess1 = check({shapeProp: {derp: 'xxx', smerp: 23}});
      assert.strictEqual(mess1, null);
      const mess2 = check({shapeProp: undefined});
      assert.strictEqual(mess2, 'PropTypes validation error at [shapeProp]: Prop isRequiredOrNull (cannot be undefined)');
      const mess3 = check({shapeProp: null});
      assert.strictEqual(mess3, null);
      const mess4 = check({shapeProp: {derp: 'derp'}});
      assert.strictEqual(mess4, 'PropTypes validation error at [shapeProp]: custom err mess');
    });

    it('shape - excess props (is not generally a failure)', () => {
      const check = checkProps({
        shapeProp: PropTypes.shape({
          blerp: PropTypes.string.isRequired,
        }),
      });
      const valids = [
        {shapeProp: undefined},
        {shapeProp: null},
        {shapeProp: {blerp: 'asdf'}},
        {shapeProp: {blerp: 'asdf', slerp: 'sksksk', derp: () => 0}},
      ];
      _.each(valids, valid => {
        const mess = check(valid);
        assert.strictEqual(mess, null);
      });
    });

  });

  describe('validator (propType)', () => {

    const shape = {
      derp: PropTypes.string.isRequired,
    };
    const validator = validateProps(shape);

    it('some valid validators', () => {
      const valids = [
        validateProps({}),
        validateProps({
          derp: PropTypes.string.isRequired,
        }),
        validateProps({
          derp: PropTypes.validator(validateProps({
            nextlevel: PropTypes.string,
          })),
        }),
        checkProps({}),
        checkProps({
          derp: PropTypes.string.isRequired,
        }),
        checkProps({
          derp: PropTypes.validator(validateProps({
            nextlevel: PropTypes.string,
          })),
        }),
        areValidProps({}),
        areValidProps({
          derp: PropTypes.string.isRequired,
        }),
        areValidProps({
          derp: PropTypes.validator(validateProps({
            nextlevel: PropTypes.string,
          })),
        }),
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps({validatorProp: PropTypes.validator(valid)}));
      });
    });

    it('some invalid propTypes', () => {
      const derp = {};
      const invalids = [
        {
          validator: null,
          err: /the validate function must be a function/,
        },
        {
          validator: () => {},
          err: /the validate function must be \(at least nominally\) a PropTypes validator, checker, or boolean tester function/,
        },
        {
          validator: (() => {
            const validator = () => {};
            validator.isPropTypesChecker = true;
            validator.internalCheck = 'NOT A FUNCTION';
            return validator;
          })(),
          err: /the validator function must include the internalCheck function/,
        },

      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({validatorProp: PropTypes.validator(invalid.validator)}), invalid.err);
      });
    });

    it('validator - default', () => {
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        })),
      });
      const checkees = [
        {props: {validatorProp: null}, mess: null},
        {props: {validatorProp: undefined}, mess: null},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator - default (isNilable - does not do anything)', () => {
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        }, {
          isNilable: true,
        })),
      });
      const checkees = [
        {props: {validatorProp: null}, mess: null},
        {props: {validatorProp: undefined}, mess: null},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator - isRequired (default})', () => {
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        })).isRequired,
      });
      const checkees = [
        {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator - isRequired (isNilable)', () => {
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        }, {
          isNilable: true,
        })).isRequired,
      });
      const checkees = [
        {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator - isRequired (with {isNilable: true})', () => {
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        }, {
          isNilable: true,
        })).isRequired,
      });
      const checkees = [
        {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator - isRequiredOrNull (with {isNilable: true})', () => {
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        }, {
          isNilable: true,
        })).isRequiredOrNull,
      });
      const checkees = [
        {props: {validatorProp: null}, mess: null},
        {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator.custom (with {isNilable: true})', () => {
      const customFn = ({prop, propName, props}) => {
        if (_.get(prop, 'derp') === 'smerp') {
          return 'custom string derpy smerp error';
        }
        if (_.get(prop, 'derp') === 'knerp') {
          return new Error('custom returned derpy knerp error');
        }
        if (_.get(prop, 'derp') === 'plerp') {
          throw new Error('custom thrown derpy plerp error');
        }
        return null;
      };
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        }, {
          isNilable: true,
        })).custom(customFn),
      });
      const checkees = [
        {props: {validatorProp: null}, mess: null},
        {props: {validatorProp: undefined}, mess: null},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
        {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
        {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
        {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator.isRequired.custom (with {isNilable: true})', () => {
      const customFn = ({prop, propName, props}) => {
        if (_.get(prop, 'derp') === 'smerp') {
          return 'custom string derpy smerp error';
        }
        if (_.get(prop, 'derp') === 'knerp') {
          return new Error('custom returned derpy knerp error');
        }
        if (_.get(prop, 'derp') === 'plerp') {
          throw new Error('custom thrown derpy plerp error');
        }
        return null;
      };
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        }, {
          isNilable: true,
        })).isRequired.custom(customFn),
      });
      const checkees = [
        {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
        {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
        {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
        {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator.isRequiredOrNull.custom (with {isNilable: true})', () => {
      const customFn = ({prop, propName, props}) => {
        if (_.get(prop, 'derp') === 'smerp') {
          return 'custom string derpy smerp error';
        }
        if (_.get(prop, 'derp') === 'knerp') {
          return new Error('custom returned derpy knerp error');
        }
        if (_.get(prop, 'derp') === 'plerp') {
          throw new Error('custom thrown derpy plerp error');
        }
        return null;
      };
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        }, {
          isNilable: true,
        })).isRequiredOrNull.custom(customFn),
      });
      const checkees = [
        {props: {validatorProp: null}, mess: null},
        {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
        {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
        {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
        {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator.custom.isRequired (with {isNilable: true})', () => {
      const customFn = ({prop, propName, props}) => {
        if (_.get(prop, 'derp') === 'smerp') {
          return 'custom string derpy smerp error';
        }
        if (_.get(prop, 'derp') === 'knerp') {
          return new Error('custom returned derpy knerp error');
        }
        if (_.get(prop, 'derp') === 'plerp') {
          throw new Error('custom thrown derpy plerp error');
        }
        return null;
      };
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        }, {
          isNilable: true,
        })).custom(customFn).isRequired,
      });
      const checkees = [
        {props: {validatorProp: null}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequired (cannot be null or undefined)'},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
        {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
        {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
        {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator.custom.isRequiredOrNull (with {isNilable: true})', () => {
      const customFn = ({prop, propName, props}) => {
        if (_.get(prop, 'derp') === 'smerp') {
          return 'custom string derpy smerp error';
        }
        if (_.get(prop, 'derp') === 'knerp') {
          return new Error('custom returned derpy knerp error');
        }
        if (_.get(prop, 'derp') === 'plerp') {
          throw new Error('custom thrown derpy plerp error');
        }
        return null;
      };
      const check = checkProps({
        validatorProp: PropTypes.validator(validateProps({
          derp: PropTypes.string,
        }, {
          isNilable: true,
        })).custom(customFn).isRequiredOrNull,
      });
      const checkees = [
        {props: {validatorProp: null}, mess: null},
        {props: {validatorProp: undefined}, mess: 'PropTypes validation error at [validatorProp]: Prop isRequiredOrNull (cannot be undefined)'},
        {props: {validatorProp: {derp: null}}, mess: null},
        {props: {validatorProp: {derp: undefined}}, mess: null},
        {props: {validatorProp: {derp: 'derp'}}, mess: null},
        {props: {validatorProp: {derp: 'smerp'}}, mess: 'PropTypes validation error at [validatorProp]: custom string derpy smerp error'},
        {props: {validatorProp: {derp: 'knerp'}}, mess: 'custom returned derpy knerp error'},
        {props: {validatorProp: {derp: 'plerp'}}, mess: 'custom thrown derpy plerp error'},
      ];
      _.each(checkees, checkee => {
        const mess = check(checkee.props);
        assert.strictEqual(mess, checkee.mess);
      });
    });

    it('validator with multiple nested occurrences', () => {

      const check0 = areValidProps({
        barp: PropTypes.string,
        carp: PropTypes.string.error(new Error('carp error')),
      }, {isNilable: true});

      const check1 = checkProps({
        jarp: PropTypes.string,
        karp: PropTypes.shape({
          larp: PropTypes.validator(check0),
        }),
      });
      const validate = validateProps({
        parp: PropTypes.validator(check1),
      });

      const invalids = [
        {
          obj: {parp: {jarp: 23}},
          mess: /PropTypes validation error at \[parp, jarp]: Prop must be a string when included/,
        },
        {
          obj: {parp: {karp: {larp: {barp: 23}}}},
          mess: /PropTypes validation error at \[parp, karp, larp, barp]: Prop must be a string when included/,
        },
        {
          obj: {parp: {karp: {larp: {carp: 23}}}},
          mess: /carp error/,
        },
      ];
      _.each(invalids, invalid => {
        assert.throws(() => validate(invalid.obj), invalid.mess);
      });
    });

  });

  describe('custom', () => {

    it('some valid and PASSING custom validation functions (i.e. allowed return values)', () => {
      const valids = [
        () => true,
        () => null,
        () => undefined,
        () => {},
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps({custom: PropTypes.custom(valid)}));
        const check = checkProps({custom: PropTypes.custom(valid)});
        const messA = check({});
        assert.strictEqual(messA, null);
        const messB = check(null);
        assert.strictEqual(messB, 'PropTypes validation error: A null object is not allowed (see options to allow)');
        const messC = check(undefined);
        assert.strictEqual(messC, 'PropTypes validation error: An undefined object is not allowed (see options to allow)');
        // with isNilable
        const check2 = checkProps({custom: PropTypes.custom(valid)}, {isNilable: true});
        const messD = check2(null);
        assert.strictEqual(messD, null);
        const messE = check2(undefined);
        assert.strictEqual(messE, null);
      });
    });

    it('some valid and FAILING custom validation functions (i.e. allowed return values)', () => {
      const valids = [
        {
          func: () => false,
          mess: 'PropTypes validation error at [custom]: Prop fails custom validation function',
        },
        {
          func: () => 'custom string message',
          mess: 'PropTypes validation error at [custom]: custom string message',
        },
        {
          func: () => {
            const err = new Error('custom error. period.');
            return err; // RETURNS error
          },
          mess: 'custom error. period.',
        },
        {
          func: () => {
            const err = new Error('custom error. period.');
            throw err; // THROWS error (no difference)
          },
          mess: 'custom error. period.',
        },
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps({custom: PropTypes.custom(valid.func)}));
        const check = checkProps({custom: PropTypes.custom(valid.func)});
        const mess = check({});
        assert.strictEqual(mess, valid.mess);
      });
    });

    it('some invalid custom validation functions (i.e. NON-allowed return values)', () => {
      const invalids = [
        () => 0,
        () => 23,
        () => ({}),
        () => [],
      ];
      _.each(invalids, invalid => {
        assert.doesNotThrow(() => checkProps({custom: PropTypes.custom(invalid)}));
        const check = checkProps({custom: PropTypes.custom(invalid)});
        const mess = check({});
        assert.strictEqual(mess, `PropTypes validation error at [custom]: Prop custom validation function returns ${invalid()} which is forbidden - for validation success return one of {true, null, undefined} and for validation failure return one of {false, <string>, <error>}`)
      });
    });

    it('some invalid custom propType', () => {
      const invalids = [
        0, // anything not a function basically
        null,
        undefined,
        [],
        {},
      ];
      const mess = /the custom function must be a function/;
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({custom: PropTypes.custom(invalid)}), mess);
      });
    });

    it('custom fn has access to {prop, propName, props}', () => {
      const leProps = {custom: 'derp'};
      const customFn = ({prop, propName, props}) => {
        assert.strictEqual(propName, 'custom');
        assert.strictEqual(prop, 'derp');
        assert.strictEqual(props, leProps)
      };
      const check = checkProps({custom: PropTypes.custom(customFn)});
      assert.doesNotThrow(() => check(leProps));
    });

    it('returning a string lets you craft custom validation messages (that still aggregate)', () => {
      const leProps = {custom: 'derp'};
      const customFn = ({prop, propName, props}) => {
        return 'custom error message';
      };
      const check = checkProps({custom: PropTypes.custom(customFn)});
      const message = check(leProps);
      assert.strictEqual(message, 'PropTypes validation error at [custom]: custom error message');
    });

    it('throwing an error => error.message becomes custom validation message', () => {
      const leProps = {custom: 'derp'};
      const customFn = ({prop, propName, props}) => {
        throw new Error('alternative way to set error message');
      };
      const check = checkProps({custom: PropTypes.custom(customFn)});
      const message = check(leProps);
      assert.strictEqual(message, 'alternative way to set error message');
    });

    it('returning a string and it gets treated as any other message: (gets a prefix and gets aggregated)', () => {
      const leProps = {custom: 'derp'};
      const customFn = ({prop, propName, props}) => 'anything can go wrong here';
      const check = checkProps({custom: PropTypes.custom(customFn)});
      const message = check(leProps);
      assert.strictEqual(message, 'PropTypes validation error at [custom]: anything can go wrong here');
    });

    it('returning an error and it gets preserved entirely (even up the hierarchy)', () => {
      const leProps = {custom: 'derp'};
      const customFn = ({prop, propName, props}) => {
        return new Error('something did go wrong');
      };
      const check = checkProps({custom: PropTypes.custom(customFn)});
      const message = check(leProps);
      assert.strictEqual(message, 'something did go wrong');
    });

    it('throwing an error is treated EXACTLY the same as just returning it', () => {
      const leProps = {custom: 'derp'};
      const customFn = ({prop, propName, props}) => {
        throw new Error('something did go wrong');
      };
      const check = checkProps({custom: PropTypes.custom(customFn)});
      const message = check(leProps);
      assert.strictEqual(message, 'something did go wrong');
    });

  });

});
