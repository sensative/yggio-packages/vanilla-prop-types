'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
} = require('../src');

describe('config input-validators', () => {

  describe('propTypes (default options)', () => {

    it('must be an object (default options)', () => {
      // not sure what the appropriate behaviour here should be
      const valids = [
        {},
        [],
        () => 0,
      ];
      _.each(valids, valid => {
        assert.doesNotThrow(() => checkProps(valid));
        assert(_.isFunction(checkProps(valid)));
      });
    });

    it('and some invalids (default options)', () => {
      const invalids = [
        0,
        'asdff',
        '',
        undefined,
        null,
      ];
      const mess = /propTypes must be an object/;
      _.each(invalids, invalid => {
        assert.throws(() => checkProps(invalid), mess);
        try {
          checkProps(invalid);
        } catch (err) {
          assert.strictEqual(err.type, 'INVALID_PROP_TYPES');
        }
      });
    });

    it('propTypes values must be propTypes functions', () => {
      const invalids = [
        {
          propTypes: {thisProp: 'not a prop-types function'},
          mess: /invalid propType function for {propName: thisProp}/,
        },
        {
          propTypes: {thisProp: () => 0},
          mess: /invalid propType function for {propName: thisProp}/,
        },
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps(invalid.propTypes), invalid.mess);
      })
    });

    it('Standard usage: props object', () => {
      const check1 = checkProps({derr: PropTypes.string});
      const mess11 = check1('asdf');
      assert.strictEqual(mess11, 'PropTypes validation error: The shape container must be a plain object (both "props" and "shape" are really synonymous to plainObject)');
      const mess12 = check1({});
      assert.strictEqual(mess12, null);
      const mess13 = check1({derr: 'asdf'});
      assert.strictEqual(mess13, null);
      const mess14 = check1({derr: 23});
      assert.strictEqual(mess14, 'PropTypes validation error at [derr]: Prop must be a string when included');
      const mess15 = check1(null);
      assert.strictEqual(mess15, 'PropTypes validation error: A null object is not allowed (see options to allow)');
      const mess16 = check1(undefined);
      assert.strictEqual(mess16, 'PropTypes validation error: An undefined object is not allowed (see options to allow)');
    });

  });

  describe('options', () => {

    it('some valids', () => {
      const valids = [
        {isNullable: true},
        {isNullable: false},
        {isNullable: undefined},
        {isNullable: null},

        {isNilable: true},
        {isNilable: false},
        {isNilable: undefined},
        {isNilable: null},

        {isExact: true},
        {isExact: false},
        {isExact: undefined},
        {isExact: null},
        {isExact: 'custom error string'},
        {isExact: new Error('custom error')},
        {isExact: () => true},
        {isExact: () => false},
        {isExact: () => undefined},
        {isExact: () => null},
        {isExact: () => 'custom error string'},
        {isExact: () => new Error('custom error')},
        {isExact: () => {
          throw new Error('custom thrown error');
        }},

        {blacklist: []},
        {blacklist: ['propA', 'propB', 'propC']},
        {custom: () => 0},
        {custom: false},
        {custom: undefined},
        {custom: null},

        {blacklistError: null},
        {blacklistError: false},
        {blacklistError: 'blacklisted prop found'},
        {blacklistError: new Error('blacklisted prop found')},
        {blacklistError: () => new Error('blacklisted prop found')},

        {objectNullError: null},
        {objectNullError: false},
        {objectNullError: 'object is null is not allowed'},
        {objectNullError: new Error('object is null is not allowed')},
        {objectNullError: () => new Error('object is null is not allowed')},

        {objectUndefinedError: null},
        {objectUndefinedError: false},
        {objectUndefinedError: 'object is null is not allowed'},
        {objectUndefinedError: new Error('object is null is not allowed')},
        {objectUndefinedError: () => new Error('object is null is not allowed')},

        {overrideError: null},
        {overrideError: false},
        {overrideError: 'override error overrides everything'},
        {overrideError: new Error('override error overrides everything')},
        {overrideError: () => new Error('override error overrides everything')},
      ];
      _.each(valids, validOptions => {
        assert.doesNotThrow(() => checkProps({}, validOptions));
      });
    });

    it('options strictly NOT allowed for any single prop validator', () => {
      const singlePropTypes = [
        PropTypes.string,
      ];
      const mess = /For the moment, options for singleton prop is not supported/;
      _.each(singlePropTypes, propType => {
        assert.throws(() => validateProps(propType, {asdf: 1}), mess);
        try {
          validateProps(propType, {asdf: 1});
        } catch (err) {
          assert.strictEqual(err.type, 'INVALID_SINGLETON_OPTIONS');
        }
      });
    });

    it('some invalids', () => {
      const invalids = [
        {
          options: {invalidKey: true},
          mess: /Invalid main options found: \[invalidKey\], validOptions = \[blacklist, blacklistError, custom, defaultError, isExact, isNilable, isNullable, objectNullError, objectUndefinedError, overrideError\]/,
          errType: 'INVALID_MAIN_OPTIONS',
        },
        {
          options: {blacklist: 23},
          mess: /Invalid blacklist, must be an array/,
          errType: 'INVALID_MAIN_BLACKLIST',
        },
        {
          options: {blacklist: [23]},
          mess: /Invalid blacklist item, must be a string: 23/,
          errType: 'INVALID_MAIN_BLACKLIST_ITEM',
        },
        {
          options: {custom: true},
          mess: /the global custom function must be a function if included/,
          errType: 'INVALID_MAIN_CUSTOM_FUNCTION',
        },
        {
          options: {isExact: 234},
          mess: /Invalid "isExact" option included. If truthy, it must be a boolean, a string \(which gets converted to an error\), an error object \(which simply gets returned\), or a function that returns or throws an error/,
          errType: 'INVALID_MAIN_OPTION_ISEXACT',
        },
        {
          options: {blacklistError: 234},
          mess: /Invalid "blacklistError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}/,
          errType: 'INVALID_MAIN_BLACKLIST_ERROR',
        },
        {
          options: {blacklistError: true},
          mess: /Invalid "blacklistError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}/,
          errType: 'INVALID_MAIN_BLACKLIST_ERROR',
        },
        {
          options: {objectNullError: true},
          mess: /Invalid "objectNullError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called without any args/,
          errType: 'INVALID_MAIN_OBJECT_NULL_ERROR',
        },
        {
          options: {objectNullError: 234},
          mess: /Invalid "objectNullError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called without any args/,
          errType: 'INVALID_MAIN_OBJECT_NULL_ERROR',
        },
        {
          options: {objectUndefinedError: true},
          mess: /Invalid "objectUndefinedError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called without any args/,
          errType: 'INVALID_MAIN_OBJECT_UNDEFINED_ERROR',
        },
        {
          options: {objectUndefinedError: 234},
          mess: /Invalid "objectUndefinedError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called without any args/,
          errType: 'INVALID_MAIN_OBJECT_UNDEFINED_ERROR',
        },
        {
          options: {overrideError: true},
          mess: /Invalid "overrideError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {metaErr}/,
          errType: 'INVALID_MAIN_OVERRIDE_ERROR',
        },
        {
          options: {overrideError: 234},
          mess: /Invalid "overrideError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {metaErr}/,
          errType: 'INVALID_MAIN_OVERRIDE_ERROR',
        },
        {
          options: {defaultError: true},
          mess: /Invalid "defaultError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {metaErr}/,
          errType: 'INVALID_MAIN_DEFAULT_ERROR',
        },
        {
          options: {defaultError: 234},
          mess: /Invalid "defaultError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {metaErr}/,
          errType: 'INVALID_MAIN_DEFAULT_ERROR',
        },
      ];
      _.each(invalids, invalid => {
        assert.throws(() => checkProps({}, invalid.options), invalid.mess);
        try {
          checkProps({}, invalid.options);
        } catch (err) {
          assert.strictEqual(err.type, invalid.errType);
        }
      });
    });

    it('isNullable option', () => {
      const options = {isNullable: true};
      const check = checkProps({derp: PropTypes.string});
      const mess1 = check({derp: 'merp'});
      assert.strictEqual(mess1, null);
    });

    it('Standard usage: props object (BUT with isNilable)', () => {
      const check1 = checkProps({derr: PropTypes.string}, {isNilable: true});
      const mess11 = check1('asdf');
      assert.strictEqual(mess11, 'PropTypes validation error: The shape container must be a plain object (both "props" and "shape" are really synonymous to plainObject)');
      const mess12 = check1({});
      assert.strictEqual(mess12, null);
      const mess13 = check1({derr: 'asdf'});
      assert.strictEqual(mess13, null);
      const mess14 = check1({derr: 23});
      assert.strictEqual(mess14, 'PropTypes validation error at [derr]: Prop must be a string when included');
      const mess15 = check1(null);
      assert.strictEqual(mess15, null);
      const mess16 = check1(undefined);
      assert.strictEqual(mess16, null);
    });

    it('custom option (triple test..)', () => {
      const custom = ({props, propName, prop}) => {
        if (_.isNil(prop)) {
          return 'inside custom function: prop is nil';
        }
        if (propName !== null) {
          throw new Error('propName should always be null');
        }
        if (props !== null) {
          throw new Error('props should always be null for global custom functions');
        }
        // only props is really relevant
        if (prop.janks && prop.janks !== 'diggity') {
          throw new Error('janks should always be diggity if included');
        }
        if (prop.bobby) {
          return 'interesting';
        }
      };
      // first the check & validate
      const check = checkProps({}, {custom});
      const validate = validateProps({}, {custom});
      // some valids
      const valids = [
        {},
        {janks: null},
        {janks: 'diggity'},
        {bobby: false},
      ];
      _.each(valids, valid => {
        const mess = check(valid);
        assert.strictEqual(mess, null);
        assert.doesNotThrow(() => validate(valid));
      });

      // and some invalids
      const invalids = [
        {props: null, mess: 'PropTypes validation error: A null object is not allowed (see options to allow)'},
        {props: undefined, mess: 'PropTypes validation error: An undefined object is not allowed (see options to allow)'},
        {props: {janks: 'NOT diggity'}, mess: 'janks should always be diggity if included'},
        {props: {janks: true}, mess: 'janks should always be diggity if included'},
        {props: {bobby: true}, mess: 'PropTypes validation error: interesting'},
      ];
      _.each(invalids, invalid => {
        const mess = check(invalid.props);
        assert.strictEqual(mess, invalid.mess);
        try {
          validate(invalid.props);
          throw new Error('We should already have thrown an error');
        } catch (err) {
          assert.strictEqual(err.message, invalid.mess);
        }
      });

      // setting {isNilable: true} custom function DOES evaluate
      const check2 = checkProps({}, {custom, isNilable: true});
      const validate2 = validateProps({}, {custom, isNilable: true});
      const invalids2 = [
        {props: null, mess: 'PropTypes validation error: inside custom function: prop is nil'},
        {props: undefined, mess: 'PropTypes validation error: inside custom function: prop is nil'},
      ];
      _.each(invalids2, invalid => {
        const mess = check2(invalid.props);
        assert.strictEqual(mess, invalid.mess);
        try {
          validate2(invalid.props);
          throw new Error('We should already have thrown an error');
        } catch (err) {
          assert.strictEqual(err.message, invalid.mess);
        }
      });
    });

    it('custom & isNilable can be used to catch nil with custom error', () => {
      const custom = () => new Error('snerp the custom error');
      const check = checkProps({derp: PropTypes.any}, {custom, isNilable: true});
      const mess1 = check(null);
      assert.strictEqual(mess1, 'snerp the custom error');
      const mess2 = check(undefined);
      assert.strictEqual(mess2, 'snerp the custom error');
    });

  });

  describe('propTypes with options exceptions', () => {

    it('null object value', () => {
      // default options triggers error
      const defaultCheck = checkProps({});
      const defaultMessage = defaultCheck(null);
      assert.strictEqual(defaultMessage, 'PropTypes validation error: A null object is not allowed (see options to allow)');
      // nullable it sails through
      const nullableCheck = checkProps({}, {isNullable: true});
      const nullableMessage = nullableCheck(null);
      assert.strictEqual(nullableMessage, null);
      // nilable it sails through
      const nilableCheck = checkProps({}, {isNilable: true});
      const nilableMessage = nilableCheck(null);
      assert.strictEqual(nilableMessage, null);
    });

    it('undefined object value', () => {
      // default options triggers error
      const defaultCheck = checkProps({});
      const defaultMessage = defaultCheck(undefined);
      assert.strictEqual(defaultMessage, 'PropTypes validation error: An undefined object is not allowed (see options to allow)');
      // nullable it ALSO triggers error
      const nullableCheck = checkProps({}, {isNullable: true});
      const nullableMessage = nullableCheck(undefined);
      assert.strictEqual(nullableMessage, 'PropTypes validation error: An undefined object is not allowed (see options to allow)');
      // // nilable it sails through
      const nilableCheck = checkProps({}, {isNilable: true});
      const nilableMessage = nilableCheck(undefined);
      assert.strictEqual(nilableMessage, null);
    });

    describe('trying to check propTypes of object with prop in blacklist fails', () => {

      it('simple blacklist', async () => {
        const blacklist = ['propA', 'propB', 'propC', 'propD'];
        const check = checkProps({}, {blacklist});
        const mess1 = check({propA: 'boo'});
        assert.strictEqual(mess1, 'PropTypes validation error: Props from blacklist found, with keys: [propA]');
        const mess2 = check({propA: 'boo', propB: 'per', propC: 'asdf'});
        assert.strictEqual(mess2, 'PropTypes validation error: Props from blacklist found, with keys: [propA, propB, propC]');
        const mess3 = check({propA: 'boo', propB: 'per', propC: 'asdf', propD: 'boop'});
        assert.strictEqual(mess3, 'PropTypes validation error: Props from blacklist found, with keys: [propA, propB, propC, ...]');
      });

      it('blacklist with string blacklistError (valid)', async () => {
        const blacklist = ['propA', 'propB', 'propC', 'propD'];
        const blacklistError = 'Invalid props found - string version';
        const expectedMess = 'PropTypes validation error: Invalid props found - string version';
        const check = checkProps({}, {blacklist, blacklistError});
        const mess1 = check({propA: 'boo'});
        assert.strictEqual(mess1, expectedMess);
        const mess2 = check({propA: 'boo', propB: 'per', propC: 'asdf'});
        assert.strictEqual(mess2, expectedMess);
        const mess3 = check({propA: 'boo', propB: 'per', propC: 'asdf', propD: 'boop'});
        assert.strictEqual(mess3, expectedMess);
      });

      it('blacklist with Error blacklistError (valid)', async () => {
        const blacklist = ['propA', 'propB', 'propC', 'propD'];
        const blacklistError = new Error('Invalid props found - error version');
        const check = checkProps({}, {blacklist, blacklistError});
        const mess1 = check({propA: 'boo'});
        assert.strictEqual(mess1, 'Invalid props found - error version');
        const mess2 = check({propA: 'boo', propB: 'per', propC: 'asdf'});
        assert.strictEqual(mess2, 'Invalid props found - error version');
        const mess3 = check({propA: 'boo', propB: 'per', propC: 'asdf', propD: 'boop'});
        assert.strictEqual(mess3, 'Invalid props found - error version');
      });

      it('blacklist with string-generating blacklistError (valid)', async () => {
        const blacklist = ['propA', 'propB', 'propC', 'propD'];
        const blacklistError = () => 'Invalid props found - generated string version';
        const expectedMess = 'PropTypes validation error: Invalid props found - generated string version';
        const check = checkProps({}, {blacklist, blacklistError});
        const mess1 = check({propA: 'boo'});
        assert.strictEqual(mess1, expectedMess);
        const mess2 = check({propA: 'boo', propB: 'per', propC: 'asdf'});
        assert.strictEqual(mess2, expectedMess);
        const mess3 = check({propA: 'boo', propB: 'per', propC: 'asdf', propD: 'boop'});
        assert.strictEqual(mess3, expectedMess);
      });

      it('blacklist with Error-Generating blacklistError (valid)', async () => {
        const blacklist = ['propA', 'propB', 'propC', 'propD'];
        const blacklistError = ({blacklistedPropsFound}) => new Error(`Invalid props found - valid error-generating version: [${blacklistedPropsFound.join(', ')}]`);
        const check = checkProps({}, {blacklist, blacklistError});
        const mess1 = check({propA: 'boo'});
        assert.strictEqual(mess1, 'Invalid props found - valid error-generating version: [propA]');
        const mess2 = check({propA: 'boo', propB: 'per', propC: 'asdf'});
        assert.strictEqual(mess2, 'Invalid props found - valid error-generating version: [propA, propB, propC]');
        const mess3 = check({propA: 'boo', propB: 'per', propC: 'asdf', propD: 'boop'});
        assert.strictEqual(mess3, 'Invalid props found - valid error-generating version: [propA, propB, propC, propD]');
      });


      it('blacklist with Error-Generating blacklistError (INVALID)', async () => {
        const blacklist = ['propA', 'propB', 'propC', 'propD'];
        const blacklistError = () => null;
        const check = checkProps({}, {blacklist, blacklistError});
        const mess1 = check({propA: 'boo'});
        assert.strictEqual(mess1, 'PropTypes validation error: Invalid "blacklistError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}). Props with blacklistedPropsFound found: [propA]');
        const mess2 = check({propA: 'boo', propB: 'per', propC: 'asdf'});
        assert.strictEqual(mess2, 'PropTypes validation error: Invalid "blacklistError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}). Props with blacklistedPropsFound found: [propA, propB, propC]');
        const mess3 = check({propA: 'boo', propB: 'per', propC: 'asdf', propD: 'boop'});
        assert.strictEqual(mess3, 'PropTypes validation error: Invalid "blacklistError" option: must be a string, an error object, or a function that returns or throws an error. The error generator gets called with {blacklistedPropsFound}). Props with blacklistedPropsFound found: [propA, propB, propC, ...]');
      });

    });

    describe('isExact option not being followed', () => {

      it('isExact = true', () => {
        const check = checkProps({
          red: PropTypes.any,
          blue: PropTypes.any,
        }, {
          isExact: true,
        });
        const invalids = [
          {props: {green: 0}, mess: 'PropTypes validation error: PropTypes.shape with isExact=true has extra (unallowed) props: [green]'},
          {props: {green: 0, black: 1, grey: 2}, mess: 'PropTypes validation error: PropTypes.shape with isExact=true has extra (unallowed) props: [green, black, grey]'},
          {props: {green: 0, black: 1, grey: 2, white: 3, purple: 4}, mess: 'PropTypes validation error: PropTypes.shape with isExact=true has extra (unallowed) props: [green, black, grey, ...]'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid.props);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('isExact = string (always valid)', () => {
        const check = checkProps({
          red: PropTypes.any,
          blue: PropTypes.any,
        }, {
          isExact: 'isExact failed - string version',
        });
        const invalids = [
          {props: {green: 0}, mess: 'PropTypes validation error: isExact failed - string version'},
          {props: {green: 0, black: 1, grey: 2}, mess: 'PropTypes validation error: isExact failed - string version'},
          {props: {green: 0, black: 1, grey: 2, white: 3, purple: 4}, mess: 'PropTypes validation error: isExact failed - string version'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid.props);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('isExact = error (always valid)', () => {
        const check = checkProps({
          red: PropTypes.any,
          blue: PropTypes.any,
        }, {
          isExact: new Error('isExact failed - string version'),
        });
        const invalids = [
          {props: {green: 0}, mess: 'isExact failed - string version'},
          {props: {green: 0, black: 1, grey: 2}, mess: 'isExact failed - string version'},
          {props: {green: 0, black: 1, grey: 2, white: 3, purple: 4}, mess: 'isExact failed - string version'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid.props);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('isExact = errorGenerator (valid - returns error)', () => {
        const check = checkProps({
          red: PropTypes.any,
          blue: PropTypes.any,
        }, {
          isExact: ({extraPropNames}) => new Error(`isExact failed - string version: [${extraPropNames.join(', ')}]`),
        });
        const invalids = [
          {props: {green: 0}, mess: 'isExact failed - string version: [green]'},
          {props: {green: 0, black: 1, grey: 2}, mess: 'isExact failed - string version: [green, black, grey]'},
          {props: {green: 0, black: 1, grey: 2, white: 3, purple: 4}, mess: 'isExact failed - string version: [green, black, grey, white, purple]'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid.props);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('isExact = errorGenerator (valid - throws error)', () => {
        const check = checkProps({
          red: PropTypes.any,
          blue: PropTypes.any,
        }, {
          isExact: ({extraPropNames}) => {
            throw new Error(`isExact failed - string version: [${extraPropNames.join(', ')}]`);
          },
        });
        const invalids = [
          {props: {green: 0}, mess: 'isExact failed - string version: [green]'},
          {props: {green: 0, black: 1, grey: 2}, mess: 'isExact failed - string version: [green, black, grey]'},
          {props: {green: 0, black: 1, grey: 2, white: 3, purple: 4}, mess: 'isExact failed - string version: [green, black, grey, white, purple]'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid.props);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('isExact = errorGenerator (INVALID - does NOT generate error)', () => {
        const check = checkProps({
          red: PropTypes.any,
          blue: PropTypes.any,
        }, {
          isExact: ({extraPropNames}) => 23,
        });
        const invalids = [
          {props: {green: 0}, mess: 'PropTypes validation error: Invalid "isExact" option, which when truthy must be a boolean, string, an error object, or a function that throws an error, or returns a boolean, string, or an error. The error generator gets called with {prop, propnName, props, extraPropNames}). Extraneous props found with isExact=errorGenerator: [green]'},
          {props: {green: 0, black: 1, grey: 2}, mess: 'PropTypes validation error: Invalid "isExact" option, which when truthy must be a boolean, string, an error object, or a function that throws an error, or returns a boolean, string, or an error. The error generator gets called with {prop, propnName, props, extraPropNames}). Extraneous props found with isExact=errorGenerator: [green, black, grey]'},
          {props: {green: 0, black: 1, grey: 2, white: 3, purple: 4}, mess: 'PropTypes validation error: Invalid "isExact" option, which when truthy must be a boolean, string, an error object, or a function that throws an error, or returns a boolean, string, or an error. The error generator gets called with {prop, propnName, props, extraPropNames}). Extraneous props found with isExact=errorGenerator: [green, black, grey, ...]'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid.props);
          assert.strictEqual(mess, invalid.mess);
        });
      });

    });

    describe('objectNullError', () => {

      it('objectNullError = string (always valid)', () => {
        const check = checkProps({}, {
          objectNullError: 'object is null - not allowed',
        });
        const mess = check(null);
        assert.strictEqual(mess, 'PropTypes validation error: object is null - not allowed');
      });

      it('objectNullError = error (always valid)', () => {
        const check = checkProps({}, {
          objectNullError: new Error('object is null - not allowed'),
        });
        const mess = check(null);
        assert.strictEqual(mess, 'object is null - not allowed');
      });

      it('objectNullError = errorGenerator (valid - return error)', () => {
        const check = checkProps({}, {
          objectNullError: () => new Error('object is null - not allowed'),
        });
        const mess = check(null);
        assert.strictEqual(mess, 'object is null - not allowed');
      });

      it('objectNullError = errorGenerator (valid - throws error)', () => {
        const check = checkProps({}, {
          objectNullError: () => {
            throw new Error('object is null - not allowed');
          },
        });
        const mess = check(null);
        assert.strictEqual(mess, 'object is null - not allowed');
      });

    });

    describe('objectUndefinedError', () => {

      it('objectUndefinedError = string (always valid)', () => {
        const check = checkProps({}, {
          objectUndefinedError: 'object is undefined - not allowed',
        });
        const mess = check(undefined);
        assert.strictEqual(mess, 'PropTypes validation error: object is undefined - not allowed');
      });

      it('objectUndefinedError = error (always valid)', () => {
        const check = checkProps({}, {
          objectUndefinedError: new Error('object is undefined - not allowed'),
        });
        const mess = check(undefined);
        assert.strictEqual(mess, 'object is undefined - not allowed');
      });

      it('objectUndefinedError = errorGenerator (valid - return error)', () => {
        const check = checkProps({}, {
          objectUndefinedError: () => new Error('object is undefined - not allowed'),
        });
        const mess = check(undefined);
        assert.strictEqual(mess, 'object is undefined - not allowed');
      });

      it('objectUndefinedError = errorGenerator (valid - throws error)', () => {
        const check = checkProps({}, {
          objectUndefinedError: () => {
            throw new Error('object is undefined - not allowed');
          },
        });
        const mess = check(undefined);
        assert.strictEqual(mess, 'object is undefined - not allowed');
      });

    });

    describe('defaultError', () => {

      it('defaultError = string (always valid)', () => {
        const check = checkProps({
          berp: PropTypes.string,
          derp: PropTypes.string.error(new Error('this error does NOT get overridden')),
        }, {
          defaultError: 'defaultError only overrides non-errors',
          isExact: true,
        });
        const invalids = [
          23,
          null,
          undefined,
          {berp: 23},
          {snerp: 'breaks isExact'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid);
          assert.strictEqual(mess, 'defaultError only overrides non-errors');
        });
        const nonOverriddenInvalids = [
          {
            val: {derp: 23},
            mess: 'this error does NOT get overridden',
          },
        ];
        _.each(nonOverriddenInvalids, invalid => {
          const mess = check(invalid.val);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('defaultError = error (always valid)', () => {
        const check = checkProps({
          berp: PropTypes.string,
          derp: PropTypes.string.error(new Error('this error does NOT get overridden')),
        }, {
          defaultError: new Error('defaultError only overrides non-errors'),
          isExact: true,
        });
        const invalids = [
          23,
          null,
          undefined,
          {berp: 23},
          {snerp: 'breaks isExact'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid);
          assert.strictEqual(mess, 'defaultError only overrides non-errors');
        });
        const nonOverriddenInvalids = [
          {
            val: {derp: 23},
            mess: 'this error does NOT get overridden',
          },
        ];
        _.each(nonOverriddenInvalids, invalid => {
          const mess = check(invalid.val);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('defaultError = error (always valid)', () => {
        const check = checkProps({
          berp: PropTypes.string,
          derp: PropTypes.string.error(new Error('this error does NOT get overridden')),
        }, {
          defaultError: new Error('defaultError only overrides non-errors'),
          isExact: true,
        });
        const invalids = [
          23,
          null,
          undefined,
          {berp: 23},
          {snerp: 'breaks isExact'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid);
          assert.strictEqual(mess, 'defaultError only overrides non-errors');
        });
        const nonOverriddenInvalids = [
          {
            val: {derp: 23},
            mess: 'this error does NOT get overridden',
          },
        ];
        _.each(nonOverriddenInvalids, invalid => {
          const mess = check(invalid.val);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('defaultError = errorGenerator (valid - return error)', () => {
        const check = checkProps({
          berp: PropTypes.string,
          derp: PropTypes.string.error(new Error('this error does NOT get overridden')),
        }, {
          defaultError: ({prop, propName, props, metaErr}) => {
            if (_.isError(metaErr)) {
              throw new Error('this error MUST NEVER get THROWN (should never get here..)');
            }
            return new Error(`defaultError only overrides non-errors. metaErr = ${metaErr.message}`);
          },
          isExact: true,
        });
        const invalids = [
          {
            val: 23,
            mess: 'defaultError only overrides non-errors. metaErr = PropTypes validation error: The shape container must be a plain object (both "props" and "shape" are really synonymous to plainObject)',
          },
          {
            val: null,
            mess: 'defaultError only overrides non-errors. metaErr = PropTypes validation error: A null object is not allowed (see options to allow)',
          },
          {
            val: undefined,
            mess: 'defaultError only overrides non-errors. metaErr = PropTypes validation error: An undefined object is not allowed (see options to allow)',
          },
          {
            val: {snerp: 'breaks isExact'},
            mess: 'defaultError only overrides non-errors. metaErr = PropTypes validation error: PropTypes.shape with isExact=true has extra (unallowed) props: [snerp]',
          },
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid.val);
          assert.strictEqual(mess, invalid.mess);
        });
        const nonOverriddenInvalids = [
          {
            val: {derp: 23},
            mess: 'this error does NOT get overridden',
          },
        ];
        _.each(nonOverriddenInvalids, invalid => {
          const mess = check(invalid.val);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('defaultError = errorGenerator (valid - throws error)', () => {
        const check = checkProps({
          berp: PropTypes.string,
          derp: PropTypes.string.error(new Error('this error does NOT get overridden')),
        }, {
          defaultError: ({metaErr}) => {
            if (_.isError(metaErr)) {
              throw new Error('this error NEVER gets returned');
            }
            throw new Error(`defaultError only overrides non-errors. metaErr = ${metaErr.message}`);
          },
          isExact: true,
        });
        const invalids = [
          {
            val: 23,
            mess: 'defaultError only overrides non-errors. metaErr = PropTypes validation error: The shape container must be a plain object (both "props" and "shape" are really synonymous to plainObject)',
          },
          {
            val: null,
            mess: 'defaultError only overrides non-errors. metaErr = PropTypes validation error: A null object is not allowed (see options to allow)',
          },
          {
            val: undefined,
            mess: 'defaultError only overrides non-errors. metaErr = PropTypes validation error: An undefined object is not allowed (see options to allow)',
          },
          {
            val: {snerp: 'breaks isExact'},
            mess: 'defaultError only overrides non-errors. metaErr = PropTypes validation error: PropTypes.shape with isExact=true has extra (unallowed) props: [snerp]',
          }
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid.val);
          assert.strictEqual(mess, invalid.mess);
        });
        const nonOverriddenInvalids = [
          {
            val: {derp: 23},
            mess: 'this error does NOT get overridden',
          },
        ];
        _.each(nonOverriddenInvalids, invalid => {
          const mess = check(invalid.val);
          assert.strictEqual(mess, invalid.mess);
        });
      });

      it('defaultError = errorGenerator (INVALID - does not generate error)', () => {
        const check = checkProps({
          berp: PropTypes.string,
        }, {
          defaultError: ({metaErr}) => 23,
          isExact: true,
        });
        const mess = check(234);
        assert.strictEqual(mess, 'Invalid "defaultError" option: must be a string, an error, or a function returns a string or an error, or throws an error. The error generator gets called with {props, metaErr}). metaErr = {"address":[],"description":"The shape container must be a plain object (both \\"props\\" and \\"shape\\" are really synonymous to plainObject)","message":"PropTypes validation error: The shape container must be a plain object (both \\"props\\" and \\"shape\\" are really synonymous to plainObject)"}}');
      });

    });

    describe('overrideError', () => {

      it('overrideError = string (always valid)', () => {
        const check = checkProps({
          berp: PropTypes.string,
          derp: PropTypes.string.error(new Error('this error gets overridden')),
        }, {
          overrideError: 'overrideError overrides everything',
          isExact: true,
        });
        const invalids = [
          23,
          null,
          undefined,
          {berp: 23},
          {derp: 23},
          {snerp: 'breaks isExact'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid);
          assert.strictEqual(mess, 'overrideError overrides everything');
        });
      });

      it('overrideError = error (always valid)', () => {
        const check = checkProps({
          berp: PropTypes.string,
          derp: PropTypes.string.error(new Error('this error gets overridden')),
        }, {
          overrideError: new Error('overrideError overrides everything'),
          isExact: true,
        });
        const invalids = [
          23,
          null,
          undefined,
          {berp: 23},
          {derp: 23},
          {snerp: 'breaks isExact'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid);
          assert.strictEqual(mess, 'overrideError overrides everything');
        });
      });

      it('overrideError = errorGenerator (valid - return error)', () => {
        const check = checkProps({
          berp: PropTypes.string,
          derp: PropTypes.string.error(new Error('this error gets overridden')),
        }, {
          // override error can implement defaults-like behaviour
          overrideError: ({metaErr}) => new Error(`overrideError overrides everything: ${metaErr.message}`),
          isExact: true,
        });
        const invalids = [
          23,
          null,
          undefined,
          {berp: 23},
          {derp: 23},
          {snerp: 'breaks isExact'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid);
          assert(/overrideError overrides everything/.test(mess));
          assert('overrideError overrides everything'.length < mess.length);
        });
      });

      it('overrideError = errorGenerator (valid - throws error)', () => {

        const check = checkProps({
          berp: PropTypes.string,
          derp: PropTypes.string.error(new Error('this error gets overridden')),
        }, {
          // override error can implement defaults-like behaviour
          overrideError: ({metaErr}) => {
            throw new Error(`overrideError overrides everything: ${metaErr.message}`)
          },
          isExact: true,
        });
        const invalids = [
          23,
          null,
          undefined,
          {berp: 23},
          {derp: 23},
          {snerp: 'breaks isExact'},
        ];
        _.each(invalids, invalid => {
          const mess = check(invalid);
          assert(/overrideError overrides everything/.test(mess));
          assert('overrideError overrides everything'.length < mess.length);
        });
      });

      it('overrideError = errorGenerator (INVALID - does not generate error)', () => {
        const check = checkProps({
          berp: PropTypes.string,
        }, {
          overrideError: ({metaErr}) => 23,
        });
        const mess = check(234);
        assert.strictEqual(mess, 'Invalid "overrideError" option: must be a string, an error, or a function returns a string or an error, or throws an error. The error generator gets called with {props, metaErr}). metaErr = {"address":[],"description":"The shape container must be a plain object (both \\"props\\" and \\"shape\\" are really synonymous to plainObject)","message":"PropTypes validation error: The shape container must be a plain object (both \\"props\\" and \\"shape\\" are really synonymous to plainObject)"}}');
      });

      it('override error wins over defaultError as well', () => {
        const check = checkProps({
          berp: PropTypes.string,
        }, {
          defaultError: new Error('defaultError loses'),
          overrideError: new Error('overrideError wins'),
        });
        const mess = check(234);
        assert.strictEqual(mess, 'overrideError wins');
      });

    });

  });

});
