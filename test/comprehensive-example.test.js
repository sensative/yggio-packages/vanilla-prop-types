'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
} = require('../src');

describe('comprehensive example', () => {

  it('basic items', () => {

    const basicItems = [
      {basicType: PropTypes.any, validValue: 'any'},
      {basicType: PropTypes.string, validValue: 'string'},
      {basicType: PropTypes.bool, validValue: false},
      {basicType: PropTypes.number, validValue: 0},
      {basicType: PropTypes.symbol, validValue: Symbol()},
      {basicType: PropTypes.func, validValue: () => 0},
      {basicType: PropTypes.object, validValue: {}},
      {basicType: PropTypes.plainObject, validValue: {}},
      {basicType: PropTypes.array, validValue: []},
      {basicType: PropTypes.oneOf(['derp']), validValue: 'derp'},
      {basicType: PropTypes.instanceOf(Date), validValue: new Date()},
      {basicType: PropTypes.arrayOf(PropTypes.any), validValue: ['any']},
      {basicType: PropTypes.objectOf(PropTypes.any), validValue: {any: 'any'}},
      {basicType: PropTypes.oneOfType([PropTypes.any]), validValue: 'any'},
      {basicType: PropTypes.shape({derp: PropTypes.any}), validValue: {derp: 'derp'}},
      {basicType: PropTypes.validator(validateProps({derp: PropTypes.any})), validValue: {derp: 'any'}},
    ];
    _.each(basicItems, item => {
      const check = checkProps({
        val: item.basicType,
        valR: item.basicType.isRequired,
        valR2: item.basicType.isRequired('custom isRequired error message'),
        valRN: item.basicType.isRequiredOrNull,
        valC: item.basicType.custom(() => null),
        valRC: item.basicType.isRequired.custom(() => null),
        valRNC: item.basicType.isRequiredOrNull.custom(() => null),
        valCR: item.basicType.custom(() => null).isRequired,
        valCRN: item.basicType.custom(() => null).isRequiredOrNull,
        valE: item.basicType.error('error'),
        valRE: item.basicType.isRequired.error('error'),
        valRNE: item.basicType.isRequiredOrNull.error('error'),
        valCE: item.basicType.custom(() => null).error('error'),
        valRCE: item.basicType.isRequired.custom(() => null).error('error'),
        valRNCE: item.basicType.isRequiredOrNull.custom(() => null).error('error'),
        valCRE: item.basicType.custom(() => null).isRequired.error('error'),
        valCRNE: item.basicType.custom(() => null).isRequiredOrNull.error('error'),
      });
      const allProps = {
        val: item.validValue,
        valR: item.validValue,
        valR2: item.validValue,
        valRN: item.validValue,
        valC: item.validValue,
        valRC: item.validValue,
        valRNC: item.validValue,
        valCR: item.validValue,
        valCRN: item.validValue,
        valE: item.validValue,
        valRE: item.validValue,
        valRNE: item.validValue,
        valCE: item.validValue,
        valRCE: item.validValue,
        valRNCE: item.validValue,
        valCRE: item.validValue,
        valCRNE: item.validValue,
      }
      const mess = check(allProps);
      assert.strictEqual(mess, null);
    });

  });

  it('the special "custom singlet"', () => {
    const check = checkProps({
      custom: PropTypes.custom(() => null),
    });
    const everythingWorks = [
      null,
      undefined,
      2,
      {},
      false,
    ];
    _.each(everythingWorks, valid => {
      const mess = check({custom: valid});
      assert.strictEqual(mess, null);
    });
  });


});
