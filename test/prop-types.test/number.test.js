'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('number', () => {

  it('number', () => {
    const check = checkProps({numberProp: PropTypes.number});
    const checkees = [
      {props: {numberProp: 'asdf'}, mess: 'PropTypes validation error at [numberProp]: Prop must be a number when included'},
      {props: {numberProp: 0}, mess: null},
      {props: {numberProp: 23}, mess: null},
      {props: {numberProp: Number(23)}, mess: null},
      {props: {numberProp: new Number(23)}, mess: null},
      {props: {numberProp: new Number('23')}, mess: null},
      {props: {}, mess: null},
      {props: {numberProp: undefined}, mess: null},
      {props: {numberProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('number.isRequired', () => {
    const check = checkProps({numberProp: PropTypes.number.isRequired});
    const checkees = [
      {props: {numberProp: 'asdf'}, mess: 'PropTypes validation error at [numberProp]: Prop must be a number when included'},
      {props: {numberProp: 0}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {numberProp: undefined}, mess: 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {numberProp: null}, mess: 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('number.isRequiredOrNull', () => {
    const check = checkProps({numberProp: PropTypes.number.isRequiredOrNull});
    const checkees = [
      {props: {numberProp: 'asdf'}, mess: 'PropTypes validation error at [numberProp]: Prop must be a number when included'},
      {props: {numberProp: 0}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [numberProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {numberProp: undefined}, mess: 'PropTypes validation error at [numberProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {numberProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('number.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 43 ? 'custom err mess' : null);
    };
    const check = checkProps({numberProp: PropTypes.number.custom(customFn)});
    const mess1 = check({numberProp: 0});
    assert.strictEqual(mess1, null);
    const mess2 = check({numberProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({numberProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({numberProp: 43});
    assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
  });

  it('number.isRequired.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 43 ? 'custom err mess' : null);
    };
    const check = checkProps({numberProp: PropTypes.number.isRequired.custom(customFn)});
    const mess1 = check({numberProp: 0});
    assert.strictEqual(mess1, null);
    const mess2 = check({numberProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({numberProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({numberProp: 43});
    assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
  });

  it('number.isRequiredOrNull.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 43 ? 'custom err mess' : null);
    };
    const check = checkProps({numberProp: PropTypes.number.isRequiredOrNull.custom(customFn)});
    const mess1 = check({numberProp: 0});
    assert.strictEqual(mess1, null);
    const mess2 = check({numberProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [numberProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({numberProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({numberProp: 43});
    assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
  });

  it('number.custom.isRequired', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 43 ? 'custom err mess' : null);
    };
    const check = checkProps({numberProp: PropTypes.number.custom(customFn).isRequired});
    const mess1 = check({numberProp: 0});
    assert.strictEqual(mess1, null);
    const mess2 = check({numberProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({numberProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [numberProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({numberProp: 43});
    assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
  });

  it('number.custom.isRequiredOrNull', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 43 ? 'custom err mess' : null);
    };
    const check = checkProps({numberProp: PropTypes.number.custom(customFn).isRequiredOrNull});
    const mess1 = check({numberProp: 0});
    assert.strictEqual(mess1, null);
    const mess2 = check({numberProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [numberProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({numberProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({numberProp: 43});
    assert.strictEqual(mess4, 'PropTypes validation error at [numberProp]: custom err mess');
  });

});
