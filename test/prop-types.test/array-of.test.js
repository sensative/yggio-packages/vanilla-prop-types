'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('arrayOf (propType)', () => {

  it('some valid propTypes', () => {
    const valids = [
      PropTypes.any,
      PropTypes.string,
      PropTypes.number.isRequired,
      PropTypes.instanceOf(Date),
      PropTypes.arrayOf(PropTypes.any.isRequired),
    ];
    _.each(valids, valid => {
      assert.doesNotThrow(() => checkProps({arrayOfProp: PropTypes.arrayOf(valid)}));
    });
  });

  it('some invalid propTypes', () => {
    const derp = {};
    const invalids = [
      {propType: {}, mess: /invalid propType function for {propName: propType}/},
      {propType: () => 0, mess: /invalid propType function for {propName: propType}/},
    ];
    _.each(invalids, invalid => {
      assert.throws(() => checkProps({arrayOfProp: PropTypes.arrayOf(invalid.propType)}), invalid.mess);
    });
  });

  it('arrayOf (PropTypes.string) - default', () => {
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string)});
    const checkees = [
      {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
      {props: {arrayOfProp: []}, mess: null},
      {props: {arrayOfProp: [null]}, mess: null},
      {props: {arrayOfProp: [undefined]}, mess: null},
      {props: {arrayOfProp: [,]}, mess: null},
      {props: {arrayOfProp: ['', 'asdf', '23', undefined, '']}, mess: null},
      {props: {}, mess: null},
      {props: {arrayOfProp: undefined}, mess: null},
      {props: {arrayOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('arrayOf (PropTypes.string.isRequired) - default', () => {
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string.isRequired)});
    const checkees = [
      {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
      {props: {arrayOfProp: []}, mess: null},
      {props: {arrayOfProp: [null]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
      {props: {arrayOfProp: [undefined]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
      {props: {arrayOfProp: [,]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequired (cannot be null or undefined)'},
      {props: {arrayOfProp: ['', 'asdf', '23', undefined, '']}, mess: 'PropTypes validation error at [arrayOfProp, 3]: Prop isRequired (cannot be null or undefined)'},
      {props: {}, mess: null},
      {props: {arrayOfProp: undefined}, mess: null},
      {props: {arrayOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('arrayOf (PropTypes.string.isRequiredOrNull) - default', () => {
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string.isRequiredOrNull)});
    const checkees = [
      {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
      {props: {arrayOfProp: []}, mess: null},
      {props: {arrayOfProp: [null]}, mess: null},
      {props: {arrayOfProp: [undefined]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {arrayOfProp: [,]}, mess: 'PropTypes validation error at [arrayOfProp, 0]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {arrayOfProp: ['', 'asdf', '23', undefined, '']}, mess: 'PropTypes validation error at [arrayOfProp, 3]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {}, mess: null},
      {props: {arrayOfProp: undefined}, mess: null},
      {props: {arrayOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('arrayOf.isRequired (any) - default', () => {
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.any).isRequired});
    const checkees = [
      {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
      {props: {arrayOfProp: []}, mess: null},
      {props: {arrayOfProp: {}}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
      {props: {}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {arrayOfProp: undefined}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {arrayOfProp: null}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('arrayOf.isRequiredOrNull (any) - default', () => {
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.any).isRequiredOrNull});
    const checkees = [
      {props: {arrayOfProp: 'asdf'}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
      {props: {arrayOfProp: []}, mess: null},
      {props: {arrayOfProp: {}}, mess: 'PropTypes validation error at [arrayOfProp]: Prop must be an array (PropTypes.arrayOf requirement)'},
      {props: {}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {arrayOfProp: undefined}, mess: 'PropTypes validation error at [arrayOfProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {arrayOfProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('arrayOf.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).custom(customFn)});
    const mess1 = check({arrayOfProp: ['xxx']});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayOfProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({arrayOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({arrayOfProp: ['derp']});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
  });

  it('arrayOf.isRequired.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).isRequired.custom(customFn)});
    const mess1 = check({arrayOfProp: ['xxx']});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({arrayOfProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({arrayOfProp: ['derp']});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
  });

  it('arrayOf.isRequiredOrNull.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).isRequiredOrNull.custom(customFn)});
    const mess1 = check({arrayOfProp: ['xxx']});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [arrayOfProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({arrayOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({arrayOfProp: ['derp']});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
  });

  it('arrayOf.custom.isRequired', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).custom(customFn).isRequired});
    const mess1 = check({arrayOfProp: ['xxx']});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({arrayOfProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [arrayOfProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({arrayOfProp: ['derp']});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
  });

  it('arrayOf.custom.isRequiredOrNull', () => {
    const customFn = ({prop, propName, props}) => {
      return (_.includes(prop, 'derp') ? 'custom err mess' : null);
    };
    const check = checkProps({arrayOfProp: PropTypes.arrayOf(PropTypes.string).custom(customFn).isRequiredOrNull});
    const mess1 = check({arrayOfProp: ['xxx']});
    assert.strictEqual(mess1, null);
    const mess2 = check({arrayOfProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [arrayOfProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({arrayOfProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({arrayOfProp: ['derp']});
    assert.strictEqual(mess4, 'PropTypes validation error at [arrayOfProp]: custom err mess');
  });

});
