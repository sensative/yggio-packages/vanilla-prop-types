'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('object', () => {

  it('object', () => {
    const check = checkProps({objectProp: PropTypes.object});
    const checkees = [
      {props: {objectProp: 'asdf'}, mess: 'PropTypes validation error at [objectProp]: Prop must be an object when included'},
      {props: {objectProp: {}}, mess: null},
      {props: {objectProp: Object()}, mess: null},
      {props: {objectProp: []}, mess: null},
      {props: {objectProp: () => 0}, mess: null},
      {props: {objectProp: async () => 0}, mess: null},
      {props: {}, mess: null},
      {props: {objectProp: undefined}, mess: null},
      {props: {objectProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('object.isRequired', () => {
    const check = checkProps({objectProp: PropTypes.object.isRequired});
    const checkees = [
      {props: {objectProp: 'asdf'}, mess: 'PropTypes validation error at [objectProp]: Prop must be an object when included'},
      {props: {objectProp: {}}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {objectProp: undefined}, mess: 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {objectProp: null}, mess: 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('object.isRequiredOrNull', () => {
    const check = checkProps({objectProp: PropTypes.object.isRequiredOrNull});
    const checkees = [
      {props: {objectProp: 'asdf'}, mess: 'PropTypes validation error at [objectProp]: Prop must be an object when included'},
      {props: {objectProp: {}}, mess: null},
      {props: {}, mess: 'PropTypes validation error at [objectProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {objectProp: undefined}, mess: 'PropTypes validation error at [objectProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {objectProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('object.custom', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({objectProp: PropTypes.object.custom(customFn)});
    const mess1 = check({objectProp: {some: 'object'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({objectProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({objectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
  });

  it('object.isRequired.custom', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({objectProp: PropTypes.object.isRequired.custom(customFn)});
    const mess1 = check({objectProp: {some: 'object'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({objectProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({objectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
  });

  it('object.isRequiredOrNull.custom', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({objectProp: PropTypes.object.isRequiredOrNull.custom(customFn)});
    const mess1 = check({objectProp: {some: 'object'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [objectProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({objectProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({objectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
  });

  it('object.custom.isRequired', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({objectProp: PropTypes.object.custom(customFn).isRequired});
    const mess1 = check({objectProp: {some: 'object'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({objectProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [objectProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({objectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
  });

  it('object.custom.isRequiredOrNull', () => {
    const errObj = {derp: 'merp'};
    const customFn = ({prop, propName, props}) => {
      return (prop === errObj ? 'custom err mess' : null);
    };
    const check = checkProps({objectProp: PropTypes.object.custom(customFn).isRequiredOrNull});
    const mess1 = check({objectProp: {some: 'object'}});
    assert.strictEqual(mess1, null);
    const mess2 = check({objectProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [objectProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({objectProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({objectProp: errObj});
    assert.strictEqual(mess4, 'PropTypes validation error at [objectProp]: custom err mess');
  });

});
