'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('string', () => {

  it('string', () => {
    const check = checkProps({stringProp: PropTypes.string});
    const mess0 = check({stringProp: 'a string should work'});
    assert.strictEqual(mess0, null);
    const mess1 = check({stringProp: new String('a String should also work')});
    assert.strictEqual(mess1, null);
    const mess2 = check({});
    assert.strictEqual(mess2, null);
    const mess3 = check({stringProp: undefined});
    assert.strictEqual(mess3, null);
    const mess4 = check({stringProp: null});
    assert.strictEqual(mess4, null);
    const mess5 = check({stringProp: 23});
    assert.strictEqual(mess5, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
    const mess6 = check({stringProp: {a: 2}});
    assert.strictEqual(mess6, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
  });

  it('string.isRequired', () => {
    const check = checkProps({stringProp: PropTypes.string.isRequired});
    const mess1 = check({stringProp: 'a string should work'});
    assert.strictEqual(mess1, null);
    const mess2 = check({});
    assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({stringProp: undefined});
    assert.strictEqual(mess3, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({stringProp: null});
    assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
    const mess5 = check({stringProp: 23});
    assert.strictEqual(mess5, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
    const mess6 = check({stringProp: {a: 2}});
    assert.strictEqual(mess6, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
  });

  it('string.isRequiredOrNull', () => {
    const check = checkProps({stringProp: PropTypes.string.isRequiredOrNull});
    const mess1 = check({stringProp: 'a string should work'});
    assert.strictEqual(mess1, null);
    const mess2 = check({});
    assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({stringProp: undefined});
    assert.strictEqual(mess3, 'PropTypes validation error at [stringProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess4 = check({stringProp: null});
    assert.strictEqual(mess4, null);
    const mess5 = check({stringProp: 23});
    assert.strictEqual(mess5, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
    const mess6 = check({stringProp: {a: 2}});
    assert.strictEqual(mess6, 'PropTypes validation error at [stringProp]: Prop must be a string when included');
  });

  it('string.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({stringProp: PropTypes.string.custom(customFn)});
    const mess1 = check({stringProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({stringProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({stringProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({stringProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
  });

  it('string.isRequired.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({stringProp: PropTypes.string.isRequired.custom(customFn)});
    const mess1 = check({stringProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({stringProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({stringProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({stringProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
  });

  it('string.isRequiredOrNull.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({stringProp: PropTypes.string.isRequiredOrNull.custom(customFn)});
    const mess1 = check({stringProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({stringProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({stringProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({stringProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
  });

  it('string.custom.isRequired', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({stringProp: PropTypes.string.custom(customFn).isRequired});
    const mess1 = check({stringProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({stringProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({stringProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [stringProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({stringProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
  });

  it('string.custom.isRequiredOrNull', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'err' ? 'custom err mess' : null);
    };
    const check = checkProps({stringProp: PropTypes.string.custom(customFn).isRequiredOrNull});
    const mess1 = check({stringProp: 'any normal string'});
    assert.strictEqual(mess1, null);
    const mess2 = check({stringProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [stringProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({stringProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({stringProp: 'err'});
    assert.strictEqual(mess4, 'PropTypes validation error at [stringProp]: custom err mess');
  });

});
