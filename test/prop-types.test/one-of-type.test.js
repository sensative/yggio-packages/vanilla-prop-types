'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  PropTypes,
  checkProps,
  validateProps,
  areValidProps,
} = require('../../src');

describe('oneOfType (propType)', () => {

  it('some valid propTypes', () => {
    const valid = [
      PropTypes.any,
      PropTypes.string,
      PropTypes.number.isRequired,
      PropTypes.instanceOf(Date),
      PropTypes.oneOfType([PropTypes.any.isRequired]),
    ];
    assert.doesNotThrow(() => checkProps({oneOfTypeProp: PropTypes.oneOfType(valid)}));
  });

  it('some invalid propTypes', () => {
    const derp = {};
    const invalids = [
      {types: null, mess: /the "oneOfType" input must be an array of PropTypes/},
      {types: {}, mess: /the "oneOfType" input must be an array of PropTypes/},
      {types: () => 0, mess: /the "oneOfType" input must be an array of PropTypes/},
      {types: [], mess: /the "oneOfType" input array must have at least one PropTypes item/},
      {types: ['a'], mess: /invalid propType function for {propName: 0}/},
    ];
    _.each(invalids, invalid => {
      assert.throws(() => checkProps({oneOfTypeProp: PropTypes.oneOfType(invalid.types)}), invalid.mess);
    });
  });

  it('oneOfType - default', () => {
    const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ])});
    const checkees = [
      {props: {oneOfTypeProp: 'asdf'}, mess: null},
      {props: {oneOfTypeProp: 23}, mess: null},
      {props: {oneOfTypeProp: () => {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
      {props: {oneOfTypeProp: {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
      {props: {}, mess: null},
      {props: {oneOfTypeProp: undefined}, mess: null},
      {props: {oneOfTypeProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('oneOfType - isRequired', () => {
    const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired});
    const checkees = [
      {props: {oneOfTypeProp: 'asdf'}, mess: null},
      {props: {oneOfTypeProp: 23}, mess: null},
      {props: {oneOfTypeProp: () => {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
      {props: {oneOfTypeProp: {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
      {props: {}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {oneOfTypeProp: undefined}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)'},
      {props: {oneOfTypeProp: null}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)'},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('oneOfType - isRequiredOrNull', () => {
    const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequiredOrNull});
    const checkees = [
      {props: {oneOfTypeProp: 'asdf'}, mess: null},
      {props: {oneOfTypeProp: 23}, mess: null},
      {props: {oneOfTypeProp: () => {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
      {props: {oneOfTypeProp: {}}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop must be one of 2 given PropTypes (PropTypes.oneOfType requirement)'},
      {props: {}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {oneOfTypeProp: undefined}, mess: 'PropTypes validation error at [oneOfTypeProp]: Prop isRequiredOrNull (cannot be undefined)'},
      {props: {oneOfTypeProp: null}, mess: null},
    ];
    _.each(checkees, checkee => {
      const mess = check(checkee.props);
      assert.strictEqual(mess, checkee.mess);
    });
  });

  it('oneOfType.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'derp' ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).custom(customFn)});
    const mess1 = check({oneOfTypeProp: 23});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfTypeProp: undefined});
    assert.strictEqual(mess2, null);
    const mess3 = check({oneOfTypeProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({oneOfTypeProp: 'derp'});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
  });

  it('oneOfType.isRequired.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'derp' ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequired.custom(customFn)});
    const mess1 = check({oneOfTypeProp: 23});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfTypeProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({oneOfTypeProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({oneOfTypeProp: 'derp'});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
  });

  it('oneOfType.isRequiredOrNull.custom', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'derp' ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).isRequiredOrNull.custom(customFn)});
    const mess1 = check({oneOfTypeProp: 23});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfTypeProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({oneOfTypeProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({oneOfTypeProp: 'derp'});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
  });

  it('oneOfType.custom.isRequired', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'derp' ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).custom(customFn).isRequired});
    const mess1 = check({oneOfTypeProp: 23});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfTypeProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)');
    const mess3 = check({oneOfTypeProp: null});
    assert.strictEqual(mess3, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequired (cannot be null or undefined)');
    const mess4 = check({oneOfTypeProp: 'derp'});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
  });

  it('oneOfType.custom.isRequiredOrNull', () => {
    const customFn = ({prop, propName, props}) => {
      return (prop === 'derp' ? 'custom err mess' : null);
    };
    const check = checkProps({oneOfTypeProp: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]).custom(customFn).isRequiredOrNull});
    const mess1 = check({oneOfTypeProp: 23});
    assert.strictEqual(mess1, null);
    const mess2 = check({oneOfTypeProp: undefined});
    assert.strictEqual(mess2, 'PropTypes validation error at [oneOfTypeProp]: Prop isRequiredOrNull (cannot be undefined)');
    const mess3 = check({oneOfTypeProp: null});
    assert.strictEqual(mess3, null);
    const mess4 = check({oneOfTypeProp: 'derp'});
    assert.strictEqual(mess4, 'PropTypes validation error at [oneOfTypeProp]: custom err mess');
  });

});
