'use strict';

const _ = require('lodash');

const assembleMetaMessage = metaErr => {
  if (!metaErr) {
    return null;
  }
  if (_.isError(metaErr)) {
    return metaErr.message;
  }
  if (!_.isObject(metaErr) || !_.isArray(metaErr.address) || !_.isString(metaErr.description)) {
    throw new Error(`DevErr: invalid metaErr obect: {metaErr: ${JSON.stringify(metaErr)}}`);
  }
  let message;
  if (!metaErr.address.length) {
    message = `PropTypes validation error: ${metaErr.description}`;
  } else {
    message = `PropTypes validation error at [${metaErr.address.join(', ')}]: ${metaErr.description}`;
  }
  return message;
};


const populateMetaMessage = metaErr => {
  if (!metaErr) {
    return null;
  }
  if (_.isError(metaErr)) {
    return metaErr;
  }
  const message = assembleMetaMessage(metaErr);
  return _.assign({}, metaErr, {message});
};


module.exports = {
  assembleMetaMessage,
  populateMetaMessage,
};
