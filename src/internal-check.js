'use strict';

const _ = require('lodash');

const {
  populateMetaMessage,
} = require('./meta-err');

const {
  MAIN_OPTIONS_KEYS,
  PROP_TYPES_IDENTIFIER_KEYS,
} = require('./constants')

const {
  validatePropTypes,
  validateSingletonOptions,
  validateMainOptions,
} = require('./validators');

const {
  PropTypes,
} = require('./prop-types');


// the main guy
const internalCheck = (propTypes, optionsRaw) => {

  // basic validation
  validatePropTypes(propTypes);
  const isSinglePropType = propTypes[PROP_TYPES_IDENTIFIER_KEYS.isPropTypesFunction];

  // 1. Simple case
  // regular PropType objects pass through unchanged
  if (isSinglePropType) {
    validateSingletonOptions(optionsRaw);
    const check = (prop) => {
      const singlePropMetaErr = propTypes({
        prop,
        props: null,
        propName: null,
      });
      return singlePropMetaErr || null;
    };
    return check;
  };

  // 2. PlainObject react-type case
  // map to shape
  validateMainOptions(optionsRaw);
  const mainOptions = optionsRaw || {};
  const shapeOptions = _.pick(mainOptions, ['isExact', 'blacklist', 'blacklistError']);


  let objectNullError = 'A null object is not allowed (see options to allow)';
  if (mainOptions[MAIN_OPTIONS_KEYS.objectNullError]) {
    objectNullError = mainOptions[MAIN_OPTIONS_KEYS.objectNullError];
    if (_.isFunction(objectNullError)) {
      try {
        objectNullError = objectNullError();
      } catch (err) {
        objectNullError = err;
      }
    }
    if (!_.isError(objectNullError) && !_.isString(objectNullError)) {
      throw new Error('DevErr: invalid resolved objectNullError from function. This should have already been caught by validation');
    }
  }


  let objectUndefinedError = 'An undefined object is not allowed (see options to allow)';
  if (mainOptions[MAIN_OPTIONS_KEYS.objectUndefinedError]) {
    objectUndefinedError = mainOptions[MAIN_OPTIONS_KEYS.objectUndefinedError];
    if (_.isFunction(objectUndefinedError)) {
      try {
        objectUndefinedError = objectUndefinedError();
      } catch (err) {
        objectUndefinedError = err;
      }
    }
    if (!_.isError(objectUndefinedError) && !_.isString(objectUndefinedError)) {
      throw new Error('DevErr: invalid resolved objectUndefinedError from function. This should have already been caught by validation');
    }
  }

  let check = PropTypes.shape(propTypes, shapeOptions);
  if (mainOptions[MAIN_OPTIONS_KEYS.isNilable]) {
    // nothing needs to be done
  } else if (mainOptions[MAIN_OPTIONS_KEYS.isNullable]) {
    check = check.isRequiredOrNull(objectUndefinedError);
  } else {
    check = check.isRequired(({prop}) => _.isNull(prop) ? objectNullError : objectUndefinedError);
  }

  if (mainOptions[MAIN_OPTIONS_KEYS.custom]) {
    const custom = mainOptions[MAIN_OPTIONS_KEYS.custom];
    check = check.custom(({prop, props, propName}) => {
      return custom({prop, propName, props});
    });
  }

  if (mainOptions[MAIN_OPTIONS_KEYS.overrideError] || mainOptions[MAIN_OPTIONS_KEYS.defaultError]) {
    // extract
    const overrideError = mainOptions[MAIN_OPTIONS_KEYS.overrideError] || null;
    const defaultError = mainOptions[MAIN_OPTIONS_KEYS.defaultError] || null;
    // amend checker
    check = check.error(({prop, propName, props, metaErr}) => {

      // keep the mutability within this disposable context
      let resolvedOverrideError = overrideError;
      let resolvedDefaultError = defaultError;


      // use overrideError if it is included
      if (overrideError) {
        if (_.isFunction(overrideError)) {
          try {
            resolvedOverrideError = overrideError({props, propName, props, metaErr});
          } catch (err) {
            resolvedOverrideError = err;
          }
        }
        if (_.isString(resolvedOverrideError)) {
          return new Error(resolvedOverrideError);
        }
        if (_.isError(resolvedOverrideError)) {
          return resolvedOverrideError;
        }
        // default is invalid
        return new Error(`Invalid "overrideError" option: must be a string, an error, or a function returns a string or an error, or throws an error. The error generator gets called with {props, metaErr}). metaErr = ${JSON.stringify(populateMetaMessage(metaErr))}}`);
      }

      // NEVER use defaultError if metaErr is already an error
      if (_.isError(metaErr)) {
        return metaErr;
      }

      // otherwise use defaultError (if appropriate)
      if (defaultError) {
        if (_.isFunction(defaultError)) {
          try {
            resolvedDefaultError = defaultError({prop, propName, props, metaErr});
          } catch (err) {
            resolvedDefaultError = err;
          }
        }
        if (_.isString(resolvedDefaultError)) {
          return new Error(resolvedDefaultError);
        }
        if (_.isError(resolvedDefaultError)) {
          return resolvedDefaultError;
        }
        // default is invalid
        return new Error(`Invalid "defaultError" option: must be a string, an error, or a function returns a string or an error, or throws an error. The error generator gets called with {props, metaErr}). metaErr = ${JSON.stringify(populateMetaMessage(metaErr))}}`);
      }

    });
  }

  // translate from external props => {props, prop, propName}
  const finalCheck = (props) => check({props: null, prop: props, propName: null});

  return finalCheck;
};

//
// Exports
//

module.exports = internalCheck;
